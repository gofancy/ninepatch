package wtf.gofancy.ninepatch.gradle.processor

import org.gradle.api.Plugin
import org.gradle.api.Project

class ProcessorContainerPlugin implements Plugin<Project> {
    @Override
    void apply(final Project project) {
        setUpBuildTask project
    }

    private static void setUpBuildTask(final Project project) {
        final buildTask = project.tasks.register 'build', ProjectContainerBuildTask
        project.subprojects.each {
            it.afterEvaluate {
                final subBuildTask = it.tasks.findByName 'build'
                buildTask.get().dependsOn subBuildTask
            }
        }
    }
}

/*
class MinecraftContainerPlugin implements Plugin<Project> {
    @Override
    void apply(final Project target) {
        setUpBuildTask target
    }

    private static void setUpBuildTask(final Project project) {
        final buildTask = project.tasks.create 'build', MinecraftContainerBuildTask

        project.subprojects.each {
            it.afterEvaluate {
                final subBuildTask = it.tasks.findByName 'build'
                buildTask.dependsOn subBuildTask
            }
        }
    }
}
 */
