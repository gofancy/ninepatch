package wtf.gofancy.ninepatch.gradle.java

import nl.javadude.gradle.plugins.license.LicenseExtension
import nl.javadude.gradle.plugins.license.LicensePlugin
import org.ajoberstar.grgit.Grgit
import org.ajoberstar.grgit.gradle.GrgitPlugin
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.JavaPlugin
import org.gradle.api.publish.maven.plugins.MavenPublishPlugin
import org.gradle.api.tasks.TaskContainer
import org.gradle.api.tasks.bundling.Jar
import org.gradle.api.tasks.testing.Test
import org.gradle.api.tasks.testing.logging.TestLoggingContainer
import org.javamodularity.moduleplugin.ModuleSystemPlugin
import org.javamodularity.moduleplugin.extensions.ModularityExtension

import java.time.Instant
import java.time.format.DateTimeFormatter

class JavaConventionsPlugin implements Plugin<Project> {

    @Override
    void apply(final Project target) {
        final extension = createJavaConventionExtension target
        fixUpProjectProperties target, extension
        applyJavaPlugin target, extension
        applyModulePlugin target, extension
        applyGitPlugin target
        applyPublishPlugin target
        applyLicensingPlugin target, extension
        setUpCommonDependencies target
    }

    private static JavaConventionsExtension createJavaConventionExtension(final Project project) {
        project.extensions.create 'ninepatch', JavaConventionsExtension
    }

    private static void fixUpProjectProperties(final Project project, final JavaConventionsExtension extension) {
        project.group = 'wtf.gofancy.ninepatch'
        project.version = "${ -> extension.projectBaseVersion.get() }${ -> computeVersion() }"
    }

    private static String computeVersion() {
        System.getProperty('BUILD_NUMBER').with { it == null? '' : "+build.$it" }
    }

    private static void applyJavaPlugin(final Project project, final JavaConventionsExtension extension) {
        project.plugins.apply JavaPlugin

        project.afterEvaluate {
            final tasks = project.tasks

            applyTestTaskSettings tasks
            applyJarTaskSettings project, tasks, extension
        }
    }

    private static void applyTestTaskSettings(final TaskContainer tasks) {
        tasks.withType(Test) { Test testTask ->
            testTask.useJUnitPlatform()
            testTask.testLogging { TestLoggingContainer container ->
                container.events 'passed', 'skipped', 'failed'
            }
        }
    }

    private static void applyJarTaskSettings(final Project project, final TaskContainer tasks, final JavaConventionsExtension extension) {
        tasks.withType(Jar) { Jar jarTask ->
            jarTask.manifest.attributes([
                    'Specification-Title' : "${ -> extension.projectId.get() }",
                    'Specification-Version' : "${ -> project.version.toString() }",
                    'Specification-Vendor' : 'Garden of Fancy',
                    'Implementation-Title' : "wtf.gofancy.${ -> extension.mainPackage.get() }",
                    'Implementation-Version' : "${ -> project.version.toString() }",
                    'Implementation-Vendor' : 'Garden of Fancy',
                    'Implementation-Timestamp' : "${ -> DateTimeFormatter.ISO_INSTANT.format Instant.now() }",
                    'Implementation-CommitTarget' : "${ -> computeCommit project  }"
            ])
        }
    }

    private static String computeCommit(final Project project) {
        try {
            final gitExtension = project.extensions.getByType Grgit
            return gitExtension.head().id
        } catch (e) {
            return "error: ${e.message}"
        }
    }

    private static void applyModulePlugin(final Project project, final JavaConventionsExtension extension) {
        project.plugins.apply ModuleSystemPlugin

        final modularity = project.extensions.getByType ModularityExtension

        project.afterEvaluate {
            modularity.moduleVersion project.version.toString()
            if (extension.enableMixedJavaVersion) {
                modularity.mixedJavaRelease 8
            }
        }
    }

    private static void applyGitPlugin(final Project project) {
        project.plugins.apply GrgitPlugin
    }

    private static void applyPublishPlugin(final Project project) {
        project.plugins.apply MavenPublishPlugin

        // TODO
    }

    private static void applyLicensingPlugin(final Project project, final JavaConventionsExtension extension) {
        project.plugins.apply LicensePlugin

        final licensingExtension = project.extensions.getByType LicenseExtension
        licensingExtension.header = getLicenseHeader project, extension
        licensingExtension.strictCheck = true
    }

    private static File getLicenseHeader(final Project project, final JavaConventionsExtension extension) {
        final headerFile = project.file 'NOTICE'
        if (headerFile.exists()) return headerFile
        if (extension.allowLicenseAsHeader.get()) {
            final licenseFile = project.file 'LICENSE'
            if (licenseFile.exists()) return licenseFile
        }
        project.rootProject == project? headerFile : getLicenseHeader(project.rootProject, extension)
    }

    private static void setUpCommonDependencies(final Project project) {
        project.repositories.add(project.repositories.mavenCentral())
        project.dependencies.add('implementation', [group: 'org.jetbrains', name: 'annotations', version: '24.0.1'])
    }
}
