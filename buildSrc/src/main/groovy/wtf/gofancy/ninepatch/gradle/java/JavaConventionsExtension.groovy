package wtf.gofancy.ninepatch.gradle.java

import org.gradle.api.provider.Property

abstract class JavaConventionsExtension {

    JavaConventionsExtension() {
        this.projectBaseVersion.convention('1.0.0')
        this.allowLicenseAsHeader.convention(true)
        this.enableMixedJavaVersion.convention(true)
    }

    abstract Property<String> getProjectId()
    abstract Property<String> getProjectBaseVersion()
    abstract Property<String> getMainPackage()
    abstract Property<Boolean> getAllowLicenseAsHeader()
    abstract Property<Boolean> getEnableMixedJavaVersion()
}
