package wtf.gofancy.ninepatch.gradle.minecraft

import org.gradle.api.Plugin
import org.gradle.api.Project

class MinecraftContainerPlugin implements Plugin<Project> {
    @Override
    void apply(final Project target) {
        setUpBuildTask target
    }

    private static void setUpBuildTask(final Project project) {
        final buildTask = project.tasks.create 'build', MinecraftContainerBuildTask

        project.subprojects.each {
            it.afterEvaluate {
                final subBuildTask = it.tasks.findByName 'build'
                buildTask.dependsOn subBuildTask
            }
        }
    }
}
