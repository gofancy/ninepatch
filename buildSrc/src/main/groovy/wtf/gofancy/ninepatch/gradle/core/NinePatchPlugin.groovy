package wtf.gofancy.ninepatch.gradle.core

import org.gradle.api.Plugin
import org.gradle.api.Project

class NinePatchPlugin implements Plugin<Project> {

    @Override
    void apply(final Project target) {
        setUpBuildTasks target
    }

    private static void setUpBuildTasks(final Project main) {
        final mainBuild = main.tasks.create 'build', NinePatchBuildTask

        main.subprojects.each {
            it.afterEvaluate {
                final buildTask = it.tasks.getByName 'build'
                mainBuild.dependsOn buildTask
            }
        }
    }
}
