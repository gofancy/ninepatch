package wtf.gofancy.ninepatch.gradle.processor

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.BasePluginExtension
import wtf.gofancy.ninepatch.gradle.java.JavaConventionsExtension
import wtf.gofancy.ninepatch.gradle.java.JavaConventionsPlugin

class ProcessorConventionsPlugin implements Plugin<Project> {
    @Override
    void apply(final Project project) {
        applyJavaConventions project
        setUpDependency project
    }

    private static void applyJavaConventions(final Project project) {
        project.plugins.apply JavaConventionsPlugin

        final extension = project.extensions.findByType JavaConventionsExtension
        extension.projectId.set "NinePatch ${project.name.capitalize()} Processor"
        extension.mainPackage.set "ninepatch.processor.${project.name.replace(['-' : ''])}"

        final baseExtension = project.extensions.findByType BasePluginExtension
        baseExtension.archivesName.set("ninepatch-processor-${project.name}")
    }

    private static void setUpDependency(final Project project) {
        final implementation = project.configurations.named 'implementation'
        final coreProject = project.rootProject.findProject ':core'
        final coreDependency = project.dependencies.create coreProject
        implementation.get().dependencies.add coreDependency
    }
}
