package wtf.gofancy.ninepatch.gradle.minecraft

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.BasePluginExtension
import org.javamodularity.moduleplugin.extensions.ModularityExtension
import org.spongepowered.gradle.vanilla.MinecraftExtension
import org.spongepowered.gradle.vanilla.VanillaGradle
import org.spongepowered.gradle.vanilla.repository.MinecraftPlatform
import wtf.gofancy.ninepatch.gradle.java.JavaConventionsExtension
import wtf.gofancy.ninepatch.gradle.java.JavaConventionsPlugin

class MinecraftTargetConventionsPlugin implements Plugin<Project> {

    @Override
    void apply(final Project target) {
        applyJavaConventions target
        applyVanillaGradle target
        setUpDependency target
        setUpModularityOverrides target
    }

    private static void applyJavaConventions(final Project project) {
        project.plugins.apply JavaConventionsPlugin

        final extension = project.extensions.findByType JavaConventionsExtension
        extension.projectId.set "NinePatch Minecraft Integration (${project.name})"
        extension.mainPackage.set 'ninepatch.mc'

        final baseExtension = project.extensions.findByType BasePluginExtension
        baseExtension.archivesName.set("ninepatch-mc-${project.name.replace(['v' : ''])}")
    }

    private static void applyVanillaGradle(final Project project) {
        project.plugins.apply VanillaGradle

        final extension = project.extensions.findByType MinecraftExtension
        extension.platform MinecraftPlatform.CLIENT
    }

    private static void setUpDependency(final Project project) {
        final implementation = project.configurations.getByName 'implementation'
        final coreProject = project.rootProject.findProject ':core'
        final coreDependency = project.dependencies.create coreProject
        implementation.dependencies.add coreDependency
    }

    private static void setUpModularityOverrides(final Project project) {
        final modularityExtension = project.extensions.getByType ModularityExtension
        modularityExtension.moduleVersion "${project.version}+mc.${project.name.replace(['v' : ''])}"
    }
}
