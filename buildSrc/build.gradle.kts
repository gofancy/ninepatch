plugins {
    `groovy-gradle-plugin`
}

java {
    toolchain.languageVersion.set(JavaLanguageVersion.of(JavaVersion.VERSION_1_8.majorVersion))
}

repositories {
    gradlePluginPortal()
    mavenCentral()
    maven {
        name = "SpongePowered Maven"
        url = uri("https://repo.spongepowered.org/repository/maven-public/")
    }
}

dependencies {
    gradleApi()
    implementation(group = "gradle.plugin.com.hierynomus.gradle.plugins", name = "license-gradle-plugin", version = "0.16.1")
    implementation(group = "org.ajoberstar.grgit", name = "grgit-core", version = "4.1.1")
    implementation(group = "org.ajoberstar.grgit", name = "grgit-gradle", version = "4.1.1")
    implementation(group = "org.javamodularity", name = "moduleplugin", version = "1.8.10")
    implementation(group = "org.spongepowered.gradle.vanilla", name = "org.spongepowered.gradle.vanilla.gradle.plugin", version = "0.2.1-SNAPSHOT") {
        isChanging = true
    }
}

gradlePlugin {
    plugins {
        create("core") {
            id = "wtf.gofancy.ninepatch.gradle.core"
            implementationClass = "wtf.gofancy.ninepatch.gradle.core.NinePatchPlugin"
        }
        create("java-conventions") {
            id = "wtf.gofancy.ninepatch.gradle.java-conventions"
            implementationClass = "wtf.gofancy.ninepatch.gradle.java.JavaConventionsPlugin"
        }
        create("processor-container") {
            id = "wtf.gofancy.ninepatch.gradle.processor-container"
            implementationClass = "wtf.gofancy.ninepatch.gradle.processor.ProcessorContainerPlugin"
        }
        create("processor-conventions") {
            id = "wtf.gofancy.ninepatch.gradle.processor-conventions"
            implementationClass = "wtf.gofancy.ninepatch.gradle.processor.ProcessorConventionsPlugin"
        }
        create("minecraft-container") {
            id = "wtf.gofancy.ninepatch.gradle.minecraft-container"
            implementationClass = "wtf.gofancy.ninepatch.gradle.minecraft.MinecraftContainerPlugin"
        }
        create("minecraft-target-conventions") {
            id = "wtf.gofancy.ninepatch.gradle.minecraft-target-conventions"
            implementationClass = "wtf.gofancy.ninepatch.gradle.minecraft.MinecraftTargetConventionsPlugin"
        }
    }
}
