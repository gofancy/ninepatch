# Android Format for NinePatch
The Android format identifies two areas: a stretchable area and a content area. The stretchable area identifies the area
that will be stretched to fit the surface if desired. The content area will determine the area that is stretched in
order to fit the specified content size.

## Specifications Data
| Name                   | Value        |
|------------------------|--------------|
| Version Tag            | `0xffffff00` |
| Sub-versions           | None         |
| Content-sizing Support | Yes          |
| Surface-sizing Support | Yes          |
