package wtf.gofancy.ninepatch.processor.android.draw;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import wtf.gofancy.ninepatch.core.data.Size;
import wtf.gofancy.ninepatch.processor.android.grid.BehavioralSize;

import java.util.Objects;

public final class DrawingBehavioralSize {
    public enum Behavior {
        STRETCH,
        FIX;

        @Contract(pure = true)
        @NotNull
        @SuppressWarnings("EnhancedSwitchMigration") // Java 8 support
        static Behavior from(@NotNull final BehavioralSize.Behavior behavior) {
            switch (behavior) {
                case STRETCH:
                    return STRETCH;
                case FIX:
                    return FIX;
                default:
                    throw new IncompatibleClassChangeError();
            }
        }
    }

    @NotNull private final Size size;
    @NotNull private final Behavior behavior;

    private DrawingBehavioralSize(@NotNull final Size size, @NotNull final Behavior behavior) {
        this.size = size;
        this.behavior = behavior;
    }

    @Contract("_ -> new")
    @NotNull
    static DrawingBehavioralSize from(@NotNull final BehavioralSize size) {
        Objects.requireNonNull(size, "size");
        return of(size.size(), Behavior.from(size.behavior()));
    }

    @Contract("_, _ -> new")
    @NotNull
    static DrawingBehavioralSize of(@NotNull final Size size, @NotNull final Behavior behavior) {
        return new DrawingBehavioralSize(
                Objects.requireNonNull(size, "size"),
                Objects.requireNonNull(behavior, "behavior")
        );
    }

    @Contract(pure = true)
    @NotNull
    public Size size() {
        return this.size;
    }

    @Contract(pure = true)
    @NotNull
    public Behavior behavior() {
        return this.behavior;
    }
}
