package wtf.gofancy.ninepatch.processor.android.draw;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import wtf.gofancy.ninepatch.core.data.Coordinates;
import wtf.gofancy.ninepatch.core.data.Size;
import wtf.gofancy.ninepatch.processor.android.grid.NinePatchGridRegion;

import java.util.Objects;

public final class DrawingGridRegion {
    @NotNull private final Coordinates topLeftCoordinates;
    @NotNull private final Size regionSizeOnImage;

    private DrawingGridRegion(@NotNull final Coordinates topLeftCoordinates, @NotNull final Size regionSizeOnImage) {
        this.topLeftCoordinates = topLeftCoordinates;
        this.regionSizeOnImage = regionSizeOnImage;
    }

    @Contract("_ -> new")
    static DrawingGridRegion from(@NotNull final NinePatchGridRegion region) {
        Objects.requireNonNull(region, "region");
        return of(region.topLeftCoordinates(), region.regionSizeOnImage());
    }

    @Contract("_, _ -> new")
    @NotNull
    static DrawingGridRegion of(@NotNull final Coordinates topLeftCoordinates, @NotNull final Size regionSizeOnImage) {
        return new DrawingGridRegion(
                Objects.requireNonNull(topLeftCoordinates, "topLeftCoordinates"),
                Objects.requireNonNull(regionSizeOnImage, "regionSizeOnImage")
        );
    }

    @Contract(pure = true)
    @NotNull
    public Coordinates topLeftCoordinates() {
        return this.topLeftCoordinates;
    }

    @Contract(pure = true)
    @NotNull
    public Size regionSizeOnImage() {
        return this.regionSizeOnImage;
    }
}
