package wtf.gofancy.ninepatch.processor.android.marking;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Range;
import wtf.gofancy.ninepatch.core.data.Coordinates;
import wtf.gofancy.ninepatch.core.data.Size;

import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;

enum MarkingSectionDirection {
    HORIZONTAL(
            Coordinates::x,
            Size::width,
            fixing1(computer(Coordinates::x, (c, x) -> Coordinates.of(x, c.y()))),
            a -> Coordinates.of(0, a)
    ),
    VERTICAL(
            Coordinates::y,
            Size::height,
            fixing1(computer(Coordinates::y, (c, y) -> Coordinates.of(c.x(), y))),
            a -> Coordinates.of(a, 0)
    );

    @NotNull private final Function<@NotNull Coordinates, @NotNull @Range(from = 0, to = Long.MAX_VALUE) Long> coordinateExtractor;
    @NotNull private final Function<@NotNull Size, @NotNull @Range(from = 0, to = Long.MAX_VALUE) Long> sizeExtractor;
    @NotNull private final Function<@NotNull Coordinates, @NotNull Coordinates> nextCoordinateComputer;
    @NotNull private final Function<@NotNull @Range(from = 0, to = Long.MAX_VALUE) Long, @NotNull Coordinates> defaultCoordinateCreator;

    MarkingSectionDirection(
            @NotNull final Function<@NotNull Coordinates, @NotNull @Range(from = 0, to = Long.MAX_VALUE) Long> coordinateExtractor,
            @NotNull final Function<@NotNull Size, @NotNull @Range(from = 0, to = Long.MAX_VALUE) Long> sizeExtractor,
            @NotNull final Function<@NotNull Coordinates, @NotNull Coordinates> nextCoordinateComputer,
            @NotNull final Function<@NotNull @Range(from = 0, to = Long.MAX_VALUE) Long, @NotNull Coordinates> defaultCoordinateCreator
    ) {
        this.coordinateExtractor = Objects.requireNonNull(coordinateExtractor, "coordinateExtractor");
        this.sizeExtractor = Objects.requireNonNull(sizeExtractor, "sizeExtractor");
        this.nextCoordinateComputer = Objects.requireNonNull(nextCoordinateComputer, "nextCoordinateComputer");
        this.defaultCoordinateCreator = Objects.requireNonNull(defaultCoordinateCreator, "defaultCoordinateCreator");
    }

    @NotNull
    private static Function<@NotNull Coordinates, @NotNull Coordinates> fixing1(
            @NotNull final BiFunction<@NotNull Coordinates, @NotNull @Range(from = 0, to = Long.MAX_VALUE) Long, @NotNull Coordinates> computer
    ) {
        Objects.requireNonNull(computer, "computer");
        return (c) -> computer.apply(c, 1L);
    }

    @NotNull
    private static BiFunction<@NotNull Coordinates, @NotNull @Range(from = 0, to = Long.MAX_VALUE) Long, @NotNull Coordinates> computer(
            @NotNull Function<@NotNull Coordinates, @NotNull @Range(from = 0, to = Long.MAX_VALUE) Long> extractor,
            @NotNull BiFunction<@NotNull Coordinates, @NotNull @Range(from = 0, to = Long.MAX_VALUE) Long, @NotNull Coordinates> creator
    ) {
        Objects.requireNonNull(extractor, "extractor");
        Objects.requireNonNull(creator, "creator");
        return (c, a) -> creator.apply(c, extractor.apply(c) + a);
    }

    @Contract(pure = true)
    @Range(from = 0, to = Long.MAX_VALUE)
    long extractCoordinate(@NotNull final Coordinates coordinates) {
        return this.coordinateExtractor.apply(Objects.requireNonNull(coordinates, "coordinates"));
    }

    @Contract(pure = true)
    @Range(from = 0, to = Long.MAX_VALUE)
    long extractSize(@NotNull final Size size) {
        return this.sizeExtractor.apply(Objects.requireNonNull(size, "size"));
    }

    @Contract(pure = true)
    @NotNull
    Coordinates nextCoordinate(@NotNull final Coordinates coordinates) {
        return this.nextCoordinateComputer.apply(Objects.requireNonNull(coordinates, "coordinates"));
    }

    @Contract(pure = true)
    @NotNull
    Coordinates defaultCoordinate(@Range(from = 0, to = Long.MAX_VALUE) final long other) {
        return this.defaultCoordinateCreator.apply(other);
    }
}
