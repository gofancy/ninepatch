package wtf.gofancy.ninepatch.processor.android.grid;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import wtf.gofancy.ninepatch.core.data.Coordinates;
import wtf.gofancy.ninepatch.core.data.Size;
import wtf.gofancy.ninepatch.processor.android.marking.ColoredMarkingSection;

import java.util.Objects;
import java.util.function.Function;

public final class NinePatchGridRegion {
    @NotNull private final GridStretchDirection stretchDirection;
    @NotNull private final Coordinates topLeftCoordinates;
    @NotNull private final Size regionSizeOnImage;

    private NinePatchGridRegion(
            @NotNull final GridStretchDirection stretchDirection,
            @NotNull final Coordinates topLeftCoordinates,
            @NotNull final Size regionSizeOnImage
    ) {
        this.stretchDirection = stretchDirection;
        this.topLeftCoordinates = topLeftCoordinates;
        this.regionSizeOnImage = regionSizeOnImage;
    }

    @Contract("_, _, _, _ -> new")
    @NotNull
    static <D> NinePatchGridRegion of(
            @NotNull final NinePatchColumnRegion columnRegion,
            @NotNull final ColoredMarkingSection<D> section,
            @NotNull final D stretchColor,
            @NotNull final D fixColor
    ) {
        return of(columnRegion, section, GridStretchDirection.stretchFix(columnRegion.kind(), stretchColor, fixColor));
    }

    @Contract("_, _, _ -> new")
    @NotNull
    static NinePatchGridRegion of(
            @NotNull final GridStretchDirection direction,
            @NotNull final Coordinates topLeftCoordinates,
            @NotNull final Size regionSizeOnImage
    ) {
        Objects.requireNonNull(direction, "direction");
        Objects.requireNonNull(topLeftCoordinates, "topLeftCoordinates");
        Objects.requireNonNull(regionSizeOnImage, "regionSizeOnImage");
        return new NinePatchGridRegion(direction, topLeftCoordinates, regionSizeOnImage);
    }

    @Contract("_, _, _ -> new")
    @NotNull
    private static <D> NinePatchGridRegion of(
            @NotNull final NinePatchColumnRegion columnRegion,
            @NotNull final ColoredMarkingSection<D> section,
            @NotNull final Function<@NotNull D, @NotNull GridStretchDirection> lookup
    ) {
        final Coordinates columnCoordinates = columnRegion.topLeftCoordinates();
        final Size columnSize = columnRegion.size();

        final GridStretchDirection direction = lookup.apply(section.color());
        final Coordinates coordinates = Coordinates.of(columnCoordinates.x(), section.displacement());
        final Size size = Size.of(columnSize.width(), section.size());
        return of(direction, coordinates, size);
    }

    @Contract(pure = true)
    @NotNull
    public GridStretchDirection stretchDirection() {
        return this.stretchDirection;
    }

    @Contract(pure = true)
    @NotNull
    public Coordinates topLeftCoordinates() {
        return this.topLeftCoordinates;
    }

    @Contract(pure = true)
    @NotNull
    public Size regionSizeOnImage() {
        return this.regionSizeOnImage;
    }
}
