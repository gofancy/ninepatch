package wtf.gofancy.ninepatch.processor.android.draw;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Range;
import org.jetbrains.annotations.Unmodifiable;
import wtf.gofancy.ninepatch.core.data.Size;

import java.util.*;
import java.util.function.LongFunction;
import java.util.function.ToLongFunction;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

public final class WeightedDrawingSizing {
    @NotNull @Unmodifiable private final List<@NotNull Size> targetSize;

    private WeightedDrawingSizing(@NotNull final List<@NotNull Size> targetSize) {
        this.targetSize = Collections.unmodifiableList(targetSize);
    }

    @Contract("_, _, _, _ -> new")
    @NotNull
    public static WeightedDrawingSizing of(
            @NotNull final List<@NotNull DrawingBehavioralSize> regions,
            @NotNull final Size targetSize,
            @NotNull final ToLongFunction<@NotNull Size> extractor,
            @NotNull final LongFunction<@NotNull Size> creator
    ) {
        final long sizeToFill = verifyFitting(regions, targetSize, extractor);

        final long[] stretchableSizes = withBehavior(regions, DrawingBehavioralSize.Behavior.STRETCH, extractor).toArray();
        final long fullStretchSize = sum(stretchableSizes);
        final double[] coveragePercentages = Arrays.stream(stretchableSizes).mapToDouble(it -> ((double) it) / fullStretchSize).toArray();

        final long[] stretchedSizes = stretchSizes(sizeToFill, fullStretchSize, coveragePercentages, stretchableSizes);
        final List<@NotNull Size> sizes = interleave(regions, stretchedSizes, creator);
        return of(sizes);
    }

    @Contract("_ -> new")
    @NotNull
    public static WeightedDrawingSizing of(@NotNull final List<@NotNull Size> sizes) {
        return new WeightedDrawingSizing(Objects.requireNonNull(sizes, "sizes"));
    }

    @Contract("_, _, _ -> _")
    @Range(from = 0, to = Long.MAX_VALUE)
    private static long verifyFitting(
            @NotNull final List<@NotNull DrawingBehavioralSize> regions,
            @NotNull final Size targetSize,
            @NotNull final ToLongFunction<@NotNull Size> extractor
    ) {
        final long maxSize = extractor.applyAsLong(targetSize);
        final long minSize = withBehavior(regions, DrawingBehavioralSize.Behavior.FIX, extractor).sum();
        if (minSize > maxSize || minSize < 0) { // Overflow
            throw new IllegalArgumentException("Unable to fit image into " + targetSize + "-sized surface: too small");
        }
        return maxSize - minSize;
    }

    @Contract("_, _, _ -> _")
    @NotNull
    private static LongStream withBehavior(
            @NotNull final List<@NotNull DrawingBehavioralSize> regions,
            @NotNull final DrawingBehavioralSize.Behavior behavior,
            @NotNull final ToLongFunction<@NotNull Size> extractor
    ) {
        return regions.stream()
                .filter(it -> it.behavior() == behavior)
                .map(DrawingBehavioralSize::size)
                .mapToLong(extractor);
    }

    @Contract("_, _, _, _ -> _")
    @Range(from = 0, to = Long.MAX_VALUE)
    private static long @NotNull [] stretchSizes(
            @Range(from = 0, to = Long.MAX_VALUE) final long newSize,
            @Range(from = 0, to = Long.MAX_VALUE) final long oldSize,
            @Range(from = 0, to = 1) @SuppressWarnings("unused") final double @NotNull [] quantityFactors, // TODO("Verify if this is needed")
            @Range(from = 0, to = Long.MAX_VALUE) final long @NotNull [] sizes
    ) {
        final int sizeQuantity = sizes.length;
        final double[] newSizes = new double[sizeQuantity];

        final double resizeFactor = 1 + (((double) newSize - oldSize) / oldSize);
        for (int i = 0; i < sizeQuantity; ++i) {
            newSizes[i] = sizes[i] * resizeFactor;
        }

        return adjustSizes(newSize, sizeQuantity, newSizes);
    }

    @Contract("_, _, _ -> _")
    @Range(from = 0, to = Long.MAX_VALUE)
    private static long @NotNull [] adjustSizes(
            @Range(from = 0, to = Long.MAX_VALUE) final long max,
            @Range(from = 0, to = Integer.MAX_VALUE) final int size,
            @Range(from = 0, to = Long.MAX_VALUE) final double @NotNull [] sizes
    ) {
        final long[] adjustedSizes = new long[size];
        for (int i = 0; i < size; ++i) {
            adjustedSizes[i] = Math.round(sizes[i]);
        }

        final long finalSize = sum(adjustedSizes);
        if (finalSize == max) {
            return adjustedSizes;
        }

        final long difference = max - finalSize;
        final int[] ascendingSizeReferences = IntStream.range(0, size)
                .boxed()
                .sorted(Comparator.comparingLong(a -> adjustedSizes[a]))
                .mapToInt(Integer::intValue)
                .toArray();

        int idx = 0;
        if (difference < 0) { // Missing some distance, add
            for (long remainingDifference = difference; remainingDifference < 0; ++remainingDifference) {
                ++adjustedSizes[ascendingSizeReferences[(idx++ % size)]];
            }
        } else { // difference > 0; too much space covered, start removing
            for (long remainingDifference = difference; remainingDifference > 0; --remainingDifference) {
                --adjustedSizes[ascendingSizeReferences[size - 1 - (idx++ % size)]];
            }
        }

        return adjustedSizes;
    }

    @Contract("_ -> _")
    private static long sum(final long @NotNull [] array) {
        return Arrays.stream(array).sum();
    }

    @Contract("_, _, _ -> _")
    @NotNull
    @Unmodifiable
    private static List<@NotNull Size> interleave(
            @NotNull final List<@NotNull DrawingBehavioralSize> regions,
            final long @NotNull [] stretchSizes,
            @NotNull final LongFunction<@NotNull Size> creator
    ) {
        final List<@NotNull Size> sizes = new ArrayList<>(regions.size());

        int stretchSizeIndex = 0;
        for (@NotNull final DrawingBehavioralSize region : regions) {
            if (region.behavior() == DrawingBehavioralSize.Behavior.STRETCH) {
                sizes.add(creator.apply(stretchSizes[stretchSizeIndex++]));
            } else {
                sizes.add(region.size());
            }
        }

        assert regions.size() == sizes.size();

        return Collections.unmodifiableList(sizes);
    }

    @Contract("_ -> _")
    @NotNull
    public Size sizeRegion(@Range(from = 0, to = Integer.MAX_VALUE) final int size) {
        return this.targetSize.get(size);
    }
}
