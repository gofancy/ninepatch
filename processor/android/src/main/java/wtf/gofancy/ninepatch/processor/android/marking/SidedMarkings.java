package wtf.gofancy.ninepatch.processor.android.marking;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Range;
import org.jetbrains.annotations.Unmodifiable;
import wtf.gofancy.ninepatch.core.data.*;
import wtf.gofancy.ninepatch.core.io.ImageSource;
import wtf.gofancy.ninepatch.core.processor.ProcessorUtilities;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public final class SidedMarkings<D> {
    @NotNull @Unmodifiable private final List<@NotNull ColoredMarkingSection<D>> top;
    @NotNull @Unmodifiable private final List<@NotNull ColoredMarkingSection<D>> left;
    @NotNull @Unmodifiable private final List<@NotNull ColoredMarkingSection<D>> right;
    @NotNull @Unmodifiable private final List<@NotNull ColoredMarkingSection<D>> bottom;

    private SidedMarkings(
        @NotNull final List<@NotNull ColoredMarkingSection<D>> top,
        @NotNull final List<@NotNull ColoredMarkingSection<D>> left,
        @NotNull final List<@NotNull ColoredMarkingSection<D>> right,
        @NotNull final List<@NotNull ColoredMarkingSection<D>> bottom
    ) {
        this.top = Collections.unmodifiableList(top);
        this.left = Collections.unmodifiableList(left);
        this.right = Collections.unmodifiableList(right);
        this.bottom = Collections.unmodifiableList(bottom);
    }

    @Contract("_ -> new")
    @NotNull
    public static <I, D> SidedMarkings<D> readSidedMarkings(@NotNull final ImageSource<I, D> source) {
        final Size imageSize = source.data().size();

        final List<@NotNull ColoredMarkingSection<D>> top = readSidedMarkings(source, MarkingSectionDirection.HORIZONTAL, 0);
        final List<@NotNull ColoredMarkingSection<D>> left = readSidedMarkings(source, MarkingSectionDirection.VERTICAL, 0);
        final List<@NotNull ColoredMarkingSection<D>> right = readSidedMarkings(source, MarkingSectionDirection.VERTICAL, imageSize.width() - 1);
        final List<@NotNull ColoredMarkingSection<D>> bottom = readSidedMarkings(source, MarkingSectionDirection.HORIZONTAL, imageSize.height() - 1);

        return SidedMarkings.of(top, left, right, bottom);
    }

    @Contract("_, _, _ -> _")
    @NotNull
    @Unmodifiable
    private static <I, D> List<@NotNull ColoredMarkingSection<D>> readSidedMarkings(
            @NotNull final ImageSource<I, D> source,
            @NotNull final MarkingSectionDirection dir,
            @Range(from = 0, to = Long.MAX_VALUE) final long initialCoordinate
    ) {
        final ImageData<I, D> data = source.data();
        final ImagePixelDataFormat<D> format = source.format();
        final D black = format.fromRgba32(ProcessorUtilities.RGBA32_BLACK);
        final D white = format.fromRgba32(ProcessorUtilities.RGBA32_WHITE);
        final ColorAlternatingMarkingBuffer<D> buffer = ColorAlternatingMarkingBuffer.directional(dir, format, black, white);
        final ImageDataProvider<D> provider = data.provider();

        // Borders have size 1, so we have to skip the first and last pixels of a row or column
        final Coordinates beginningCoordinate = dir.nextCoordinate(dir.defaultCoordinate(initialCoordinate));
        final long maxCoordinate = dir.extractSize(data.size()) - 1;
        for (Coordinates c = beginningCoordinate; dir.extractCoordinate(c) < maxCoordinate; c = dir.nextCoordinate(c)) {
            buffer.then(provider.formattedPixelAt(c));
        }

        return buffer.sections();
    }

    @Contract("_, _, _, _ -> new")
    @NotNull
    private static <D> SidedMarkings<D> of(
            @NotNull final List<ColoredMarkingSection<D>> top,
            @NotNull final List<ColoredMarkingSection<D>> left,
            @NotNull final List<ColoredMarkingSection<D>> right,
            @NotNull final List<ColoredMarkingSection<D>> bottom
    ) {
        return new SidedMarkings<>(
                Objects.requireNonNull(top, "top"),
                Objects.requireNonNull(left, "left"),
                Objects.requireNonNull(right, "right"),
                Objects.requireNonNull(bottom, "bottom")
        );
    }

    @Contract(pure = true)
    @NotNull
    @Unmodifiable
    public List<ColoredMarkingSection<D>> top() {
        return this.top;
    }

    @Contract(pure = true)
    @NotNull
    @Unmodifiable
    public List<ColoredMarkingSection<D>> left() {
        return this.left;
    }

    @Contract(pure = true)
    @NotNull
    @Unmodifiable
    public List<ColoredMarkingSection<D>> right() {
        return this.right;
    }

    @Contract(pure = true)
    @NotNull
    @Unmodifiable
    public List<ColoredMarkingSection<D>> bottom() {
        return this.bottom;
    }
}
