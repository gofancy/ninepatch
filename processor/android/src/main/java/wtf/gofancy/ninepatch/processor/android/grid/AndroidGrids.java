package wtf.gofancy.ninepatch.processor.android.grid;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import wtf.gofancy.ninepatch.core.data.ImagePixelDataFormat;
import wtf.gofancy.ninepatch.core.data.Size;
import wtf.gofancy.ninepatch.core.io.ImageSource;
import wtf.gofancy.ninepatch.processor.android.marking.MeaningfulMarkings;

public final class AndroidGrids {
    @NotNull private final NinePatchGrid stretchingGrid;
    @NotNull private final NinePatchGrid contentPositioning;

    private AndroidGrids(@NotNull final NinePatchGrid stretchingGrid, @NotNull final NinePatchGrid contentPositioning) {
        this.stretchingGrid = stretchingGrid;
        this.contentPositioning = contentPositioning;
    }

    @Contract("_, _ -> new")
    @NotNull
    public static <I, D> AndroidGrids fromMarkings(
            @NotNull final MeaningfulMarkings<D> markings,
            @NotNull final ImageSource<I, D> source
    ) {
        final ImagePixelDataFormat<D> format = source.format();
        final Size imageSize = source.data().size();

        final NinePatchGrid stretchingGrid = NinePatchGrid.from(markings.horizontalStretch(), markings.verticalStretch(), format, imageSize);
        final NinePatchGrid contentGrid = NinePatchGrid.from(markings.horizontalContentArea(), markings.verticalContentArea(), format, imageSize);
        return new AndroidGrids(stretchingGrid, contentGrid);
    }

    @Contract(pure = true)
    @NotNull
    public NinePatchGrid stretchingGrid() {
        return this.stretchingGrid;
    }

    @Contract(pure = true)
    @NotNull
    public NinePatchGrid contentPositioning() {
        return this.contentPositioning;
    }
}
