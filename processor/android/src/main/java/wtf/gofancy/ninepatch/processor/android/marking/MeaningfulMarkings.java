package wtf.gofancy.ninepatch.processor.android.marking;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.Unmodifiable;

import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

public abstract class MeaningfulMarkings<D> {
    private static abstract class SealedJava8HierarchyHelper {
        SealedJava8HierarchyHelper() {}
    }

    private static final class Sided<D> extends MeaningfulMarkings<D> {
        @NotNull private final SidedMarkings<D> sidedMarkings;

        Sided(@NotNull final SidedMarkings<D> sidedMarkings) {
            super(new SealedJava8HierarchyHelper() {});
            this.sidedMarkings = Objects.requireNonNull(sidedMarkings, "sidedMarkings");
        }

        @Contract(pure = true)
        @NotNull
        @Override
        @Unmodifiable
        public List<@NotNull ColoredMarkingSection<D>> horizontalStretch() {
            return this.sidedMarkings.top();
        }

        @Contract(pure = true)
        @NotNull
        @Override
        @Unmodifiable
        public List<@NotNull ColoredMarkingSection<D>> verticalStretch() {
            return this.sidedMarkings.left();
        }

        @Contract(pure = true)
        @NotNull
        @Override
        @Unmodifiable
        public List<@NotNull ColoredMarkingSection<D>> verticalContentArea() {
            return this.sidedMarkings.right();
        }

        @Contract(pure = true)
        @NotNull
        @Override
        @Unmodifiable
        public List<@NotNull ColoredMarkingSection<D>> horizontalContentArea() {
            return this.sidedMarkings.bottom();
        }
    }

    private static final class Validated<D> extends MeaningfulMarkings<D> {
        @NotNull private final Supplier<@NotNull List<@NotNull ColoredMarkingSection<D>>> horizontalStretchArea;
        @NotNull private final Supplier<@NotNull List<@NotNull ColoredMarkingSection<D>>> verticalStretchArea;
        @NotNull private final Supplier<@NotNull List<@NotNull ColoredMarkingSection<D>>> horizontalContentArea;
        @NotNull private final Supplier<@NotNull List<@NotNull ColoredMarkingSection<D>>> verticalContentArea;

        Validated(
                @NotNull final Supplier<@NotNull List<@NotNull ColoredMarkingSection<D>>> horizontalStretchArea,
                @NotNull final Supplier<@NotNull List<@NotNull ColoredMarkingSection<D>>> verticalStretchArea,
                @NotNull final Supplier<@NotNull List<@NotNull ColoredMarkingSection<D>>> horizontalContentArea,
                @NotNull final Supplier<@NotNull List<@NotNull ColoredMarkingSection<D>>> verticalContentArea
        ) {
            super(new SealedJava8HierarchyHelper() {});
            this.horizontalStretchArea = Objects.requireNonNull(horizontalStretchArea, "horizontalStretchArea");
            this.verticalStretchArea = Objects.requireNonNull(verticalStretchArea, "verticalStretchArea");
            this.horizontalContentArea = Objects.requireNonNull(horizontalContentArea, "horizontalContentArea");
            this.verticalContentArea = Objects.requireNonNull(verticalContentArea, "verticalContentArea");
        }

        @Contract(pure = true)
        @NotNull
        @Override
        @Unmodifiable
        public List<@NotNull ColoredMarkingSection<D>> horizontalStretch() {
            return this.horizontalStretchArea.get();
        }

        @Contract(pure = true)
        @NotNull
        @Override
        @Unmodifiable
        public List<@NotNull ColoredMarkingSection<D>> verticalStretch() {
            return this.verticalStretchArea.get();
        }

        @Contract(pure = true)
        @NotNull
        @Override
        @Unmodifiable
        public List<@NotNull ColoredMarkingSection<D>> verticalContentArea() {
            return this.verticalContentArea.get();
        }

        @Contract(pure = true)
        @NotNull
        @Override
        @Unmodifiable
        public List<@NotNull ColoredMarkingSection<D>> horizontalContentArea() {
            return this.horizontalContentArea.get();
        }
    }

    MeaningfulMarkings(@NotNull final SealedJava8HierarchyHelper sealedJava8HierarchyHelper) {
        Objects.requireNonNull(sealedJava8HierarchyHelper, "sealedJava8HierarchyHelper");
    }

    @Contract("_ -> new")
    @NotNull
    public static <D> MeaningfulMarkings<D> from(@NotNull final SidedMarkings<D> markings) {
        return new Sided<>(Objects.requireNonNull(markings, "markings"));
    }

    @Contract("_, _, _, _, _ -> new")
    @NotNull
    public static <D> MeaningfulMarkings<D> validated(
            @NotNull final MeaningfulMarkings<D> older,
            @Nullable final List<@NotNull ColoredMarkingSection<D>> validatedHorizontalStretchArea,
            @Nullable final List<@NotNull ColoredMarkingSection<D>> validatedVerticalStretchArea,
            @Nullable final List<@NotNull ColoredMarkingSection<D>> validatedHorizontalContentArea,
            @Nullable final List<@NotNull ColoredMarkingSection<D>> validatedVerticalContentArea
    ) {
        return new Validated<>(
                supplyOrDefault(validatedHorizontalStretchArea, older::horizontalStretch),
                supplyOrDefault(validatedVerticalStretchArea, older::verticalStretch),
                supplyOrDefault(validatedHorizontalContentArea, older::horizontalContentArea),
                supplyOrDefault(validatedVerticalContentArea, older::verticalContentArea)
        );
    }

    @Contract("_, _ -> new")
    @NotNull
    private static <D> Supplier<@NotNull List<@NotNull ColoredMarkingSection<D>>> supplyOrDefault(
            @Nullable final List<@NotNull ColoredMarkingSection<D>> maybeNullMarkings,
            @NotNull final Supplier<@NotNull List<@NotNull ColoredMarkingSection<D>>> defaultMarkings
    ) {
        return maybeNullMarkings == null? defaultMarkings : () -> maybeNullMarkings;
    }

    @Contract(pure = true)
    @NotNull
    @Unmodifiable
    public abstract List<@NotNull ColoredMarkingSection<D>> horizontalStretch();

    @Contract(pure = true)
    @NotNull
    @Unmodifiable
    public abstract List<@NotNull ColoredMarkingSection<D>> verticalStretch();

    @Contract(pure = true)
    @NotNull
    @Unmodifiable
    public abstract List<@NotNull ColoredMarkingSection<D>> verticalContentArea();

    @Contract(pure = true)
    @NotNull
    @Unmodifiable
    public abstract List<@NotNull ColoredMarkingSection<D>> horizontalContentArea();

}
