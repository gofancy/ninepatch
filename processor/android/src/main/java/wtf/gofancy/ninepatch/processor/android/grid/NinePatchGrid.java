package wtf.gofancy.ninepatch.processor.android.grid;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Unmodifiable;
import wtf.gofancy.ninepatch.core.data.ImagePixelDataFormat;
import wtf.gofancy.ninepatch.core.data.Size;
import wtf.gofancy.ninepatch.core.processor.ProcessorUtilities;
import wtf.gofancy.ninepatch.processor.android.marking.ColoredMarkingSection;

import java.util.Collections;
import java.util.List;
import java.util.function.LongFunction;
import java.util.stream.Collectors;

public final class NinePatchGrid {
    @NotNull @Unmodifiable private final List<@NotNull BehavioralSize> columnSizes;
    @NotNull @Unmodifiable private final List<@NotNull BehavioralSize> rowSizes;
    @NotNull @Unmodifiable private final List<@NotNull NinePatchGridRegion> regions;

    private NinePatchGrid(
            @NotNull final List<@NotNull BehavioralSize> columnSizes,
            @NotNull final List<@NotNull BehavioralSize> rowSizes,
            @NotNull final List<@NotNull NinePatchGridRegion> regions
    ) {
        this.columnSizes = Collections.unmodifiableList(columnSizes);
        this.rowSizes = Collections.unmodifiableList(rowSizes);
        this.regions = Collections.unmodifiableList(regions);
    }

    @Contract("_, _, _, _ -> new")
    @NotNull
    public static <D> NinePatchGrid from(
            @NotNull final List<@NotNull ColoredMarkingSection<D>> horizontal,
            @NotNull final List<@NotNull ColoredMarkingSection<D>> vertical,
            @NotNull final ImagePixelDataFormat<D> format,
            @NotNull final Size imageSize
    ) {
        final D black = format.fromRgba32(ProcessorUtilities.RGBA32_BLACK);
        final D white = format.fromRgba32(ProcessorUtilities.RGBA32_WHITE);
        final List<@NotNull BehavioralSize> columnSizes = makeBehavioralSizes(horizontal, it -> Size.of(it, 0), black, white);
        final List<@NotNull BehavioralSize> rowSizes = makeBehavioralSizes(vertical, it -> Size.of(0, it), black, white);
        final List<@NotNull NinePatchColumnRegion> columns = makeColumns(horizontal, black, white, imageSize);
        final List<@NotNull NinePatchGridRegion> regions = regionsFromColumns(vertical, columns, black, white);
        return new NinePatchGrid(columnSizes, rowSizes, regions);
    }

    @Contract("_, _, _, _ -> new")
    @NotNull
    private static <D> List<@NotNull BehavioralSize> makeBehavioralSizes(
            @NotNull final List<@NotNull ColoredMarkingSection<D>> markings,
            @NotNull final LongFunction<@NotNull Size> sizeCreator,
            @NotNull final D stretchColor,
            @NotNull final D fixColor
    ) {
        return markings.stream()
                .map(it -> BehavioralSize.of(it, sizeCreator, stretchColor, fixColor))
                .collect(Collectors.toList());
    }

    @Contract("_, _, _, _ -> new")
    @NotNull
    private static <D> List<@NotNull NinePatchColumnRegion> makeColumns(
            @NotNull final List<@NotNull ColoredMarkingSection<D>> horizontalMarkings,
            @NotNull final D stretchColor,
            @NotNull final D fixColor,
            @NotNull final Size imageSize
    ) {
        return horizontalMarkings.stream()
                .map(it -> NinePatchColumnRegion.of(it, stretchColor, fixColor, imageSize))
                .collect(Collectors.toList());
    }

    @Contract("_, _, _, _ -> new")
    @NotNull
    private static <D> List<@NotNull NinePatchGridRegion> regionsFromColumns(
            @NotNull final List<@NotNull ColoredMarkingSection<D>> vertical,
            @NotNull final List<@NotNull NinePatchColumnRegion> columns,
            @NotNull final D stretchColor,
            @NotNull final D fixColor
    ) {
        return columns.stream()
                .map(it -> regionsFromColumn(vertical, it, stretchColor, fixColor))
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

    @Contract("_, _, _, _ -> new")
    @NotNull
    private static <D> List<@NotNull NinePatchGridRegion> regionsFromColumn(
            @NotNull final List<@NotNull ColoredMarkingSection<D>> vertical,
            @NotNull final NinePatchColumnRegion column,
            @NotNull final D stretchColor,
            @NotNull final D fixColor
    ) {
        return vertical.stream()
                .map(it -> NinePatchGridRegion.of(column, it, stretchColor, fixColor))
                .collect(Collectors.toList());
    }

    @Contract(pure = true)
    @NotNull
    @Unmodifiable
    public List<@NotNull BehavioralSize> horizontalSizes() {
        return this.columnSizes;
    }

    @Contract(pure = true)
    @NotNull
    @Unmodifiable
    public List<@NotNull BehavioralSize> verticalSizes() {
        return this.rowSizes;
    }

    @Contract(pure = true)
    @NotNull
    @Unmodifiable
    public List<@NotNull NinePatchGridRegion> regions() {
        return this.regions;
    }
}
