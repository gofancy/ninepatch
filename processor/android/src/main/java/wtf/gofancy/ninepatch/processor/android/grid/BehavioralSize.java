package wtf.gofancy.ninepatch.processor.android.grid;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import wtf.gofancy.ninepatch.core.data.Size;
import wtf.gofancy.ninepatch.processor.android.marking.ColoredMarkingSection;

import java.util.Objects;
import java.util.function.Function;
import java.util.function.LongFunction;

public final class BehavioralSize {
    public enum Behavior {
        STRETCH,
        FIX;

        @Contract(pure = true)
        @NotNull
        static <D> Function<@NotNull D, @NotNull Behavior> stretchFix(@NotNull final D stretch, @NotNull final D fix) {
            Objects.requireNonNull(stretch, "stretch");
            Objects.requireNonNull(fix, "fix");
            final Function<@NotNull D, @Nullable Behavior> lookup = it -> Objects.equals(it, stretch)? STRETCH : Objects.equals(it, fix)? FIX : null;
            return it -> Objects.requireNonNull(lookup.apply(it), "Invalid color given: neither stretch nor fix");
        }
    }

    @NotNull private final Size size;
    @NotNull private final Behavior behavior;

    private BehavioralSize(@NotNull final Size size, @NotNull final Behavior behavior) {
        this.size = size;
        this.behavior = behavior;
    }

    @Contract("_, _, _, _ -> new")
    @NotNull
    static <D> BehavioralSize of(
            @NotNull final ColoredMarkingSection<D> section,
            @NotNull final LongFunction<@NotNull Size> sizeCreator,
            @NotNull final D stretchColor,
            @NotNull final D fixColor
    ) {
        Objects.requireNonNull(section, "section");
        Objects.requireNonNull(sizeCreator, "sizeCreator");
        return of(section, sizeCreator, Behavior.stretchFix(stretchColor, fixColor));
    }

    @Contract("_, _, _ -> new")
    @NotNull
    private static <D> BehavioralSize of(
            @NotNull final ColoredMarkingSection<D> section,
            @NotNull final LongFunction<@NotNull Size> sizeCreator,
            @NotNull final Function<@NotNull D, @NotNull Behavior> lookup
    ) {
        final Behavior behavior = lookup.apply(section.color());
        final Size size = sizeCreator.apply(section.size());
        return size(size, behavior);
    }

    @Contract("_, _ -> new")
    @NotNull
    private static BehavioralSize size(@NotNull final Size size, @NotNull final Behavior behavior) {
        return new BehavioralSize(
                Objects.requireNonNull(size, "size"),
                Objects.requireNonNull(behavior, "behavior")
        );
    }

    @Contract(pure = true)
    @NotNull
    public Size size() {
        return this.size;
    }

    @Contract(pure = true)
    @NotNull
    public Behavior behavior() {
        return this.behavior;
    }
}
