package wtf.gofancy.ninepatch.processor.android.draw;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Range;
import wtf.gofancy.ninepatch.core.data.Coordinates;
import wtf.gofancy.ninepatch.core.data.Size;

import java.util.*;
import java.util.function.Function;
import java.util.function.LongFunction;
import java.util.function.ToLongFunction;
import java.util.stream.Stream;

public final class MergedGridComputer {
    @SuppressWarnings("ClassCanBeRecord") // Java 8
    private static final class PositionedDrawingBehavioralSize {
        @NotNull private final Object id;
        @NotNull private final DrawingBehavioralSize.Behavior behavior;
        @Range(from = 0, to = Long.MAX_VALUE) private final long position;
        @Range(from = 0, to = Long.MAX_VALUE) private final long size;

        private PositionedDrawingBehavioralSize(
                @NotNull final Object id,
                @NotNull final DrawingBehavioralSize.Behavior behavior,
                @Range(from = 0, to = Long.MAX_VALUE) final long position,
                @Range(from = 0, to = Long.MAX_VALUE) final long size
        ) {
            this.id = id;
            this.behavior = behavior;
            this.position = position;
            this.size = size;
        }

        @Contract(pure = true, value = "_, _, _, _ -> new")
        @NotNull
        static PositionedDrawingBehavioralSize of(
                @NotNull final Object id,
                @NotNull final DrawingBehavioralSize.Behavior behavior,
                @Range(from = 0, to = Long.MAX_VALUE) final long position,
                @Range(from = 0, to = Long.MAX_VALUE) final long size
        ) {
            return new PositionedDrawingBehavioralSize(id, behavior, position, size);
        }

        @Contract(pure = true)
        @NotNull
        Object id() {
            return this.id;
        }

        @Contract(pure = true)
        @NotNull
        DrawingBehavioralSize.Behavior behavior() {
            return this.behavior;
        }

        @Contract(pure = true)
        @Range(from = 0, to = Long.MAX_VALUE)
        long position() {
            return this.position;
        }

        @Contract(pure = true)
        @Range(from = 0, to = Long.MAX_VALUE)
        long size() {
            return this.size;
        }

        @Contract(pure = true, value = "null -> false")
        @Override
        public boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || this.getClass() != o.getClass()) return false;
            final PositionedDrawingBehavioralSize that = (PositionedDrawingBehavioralSize) o;
            return this.id == that.id;
        }

        @Contract(pure = true)
        @Override
        public int hashCode() {
            return Objects.hash(this.id);
        }
    }

    @NotNull private final DrawingGrid stretchingGrid;
    @NotNull private final DrawingGrid contentGrid;

    private MergedGridComputer(@NotNull final DrawingGrid stretchingGrid, @NotNull final DrawingGrid contentGrid) {
        this.stretchingGrid = stretchingGrid;
        this.contentGrid = contentGrid;
    }

    @Contract("_, _ -> new")
    @NotNull
    public static MergedGridComputer of(@NotNull final DrawingGrid stretchingGrid, @NotNull final DrawingGrid contentGrid) {
        return new MergedGridComputer(
                Objects.requireNonNull(stretchingGrid, "stretchingGrid"),
                Objects.requireNonNull(contentGrid, "contentGrid")
        );
    }

    @Contract("_ -> _")
    @NotNull
    public DrawingGrid buildGrid(@NotNull final List<@NotNull Size> contentSizes) {
        Objects.requireNonNull(contentSizes, "contentSizes");
        final DrawingGrid regionalStretchAreas = this.doFirstMergePass();
        return this.recursivelyApplySizing(regionalStretchAreas, contentSizes);
    }

    @Contract("_ -> _")
    @NotNull
    public Size computeSize(@NotNull final DrawingGrid grid) {
        return this.computeSize(grid, 0, 0);
    }

    @Contract(pure = true)
    @NotNull
    private DrawingGrid doFirstMergePass() {
        final List<@NotNull DrawingBehavioralSize> horizontal = this.mergeSizes(
                this.contentGrid.horizontalSizes(),
                this.stretchingGrid.horizontalSizes(),
                Size::width,
                a -> Size.of(a, 0)
        );
        final List<@NotNull DrawingBehavioralSize> vertical = this.mergeSizes(
                this.contentGrid.verticalSizes(),
                this.stretchingGrid.verticalSizes(),
                Size::height,
                a -> Size.of(0, a)
        );
        final List<@NotNull DrawingGridRegion> regions = this.createRegions(horizontal, vertical);
        return DrawingGrid.of(horizontal, vertical, regions);
    }

    @Contract(pure = true)
    @NotNull
    private List<@NotNull DrawingBehavioralSize> mergeSizes(
            @NotNull final List<@NotNull DrawingBehavioralSize> content,
            @NotNull final List<@NotNull DrawingBehavioralSize> stretch,
            @NotNull final ToLongFunction<@NotNull Size> sizeExtractor,
            @Range(from = 0, to = Long.MAX_VALUE) @NotNull final LongFunction<@NotNull Size> sizeCreator
    ) {
        final List<@NotNull PositionedDrawingBehavioralSize> positionedContent = this.toPositioned(content, sizeExtractor);
        final List<@NotNull PositionedDrawingBehavioralSize> positionedStretch = this.toPositioned(stretch, sizeExtractor);
        final List<@NotNull PositionedDrawingBehavioralSize> positionedResults = new ArrayList<>();

        for (@NotNull final PositionedDrawingBehavioralSize contentElement : positionedContent) {
            // Fixed on content means that space is not allowed for content to be put in there, therefore we can just
            // assume that the resulting region will be as big as defined. If any stretching regions are within, they
            // will be ignored as they do not participate in sizing of the content area
            if (contentElement.behavior() == DrawingBehavioralSize.Behavior.FIX) {
                positionedResults.add(contentElement);
                continue;
            }

            // This is a stretching region, so we need to first find out what regions exactly intersect and go from
            // there. It is important to note the regions in this list are not trimmed, as further processing is
            // required to preserve sizing. Note also that there is guaranteed to be at least one element in this list,
            // as the size of any content region is at least 1 (at least 1 pixel must be colored in for this to be a
            // stretch region).
            final List<@NotNull PositionedDrawingBehavioralSize> intersecting = this.intersect(positionedStretch, contentElement);
            assert intersecting.size() > 0;

            // Now, out of all the intersecting regions, we cut and match as needed to obtain the new regions
            // The general situation is essentially the following:
            // <-----><-------><----->
            //      <-------------->
            //  beginning         ending
            // We can therefore compute the coordinates essentially of all intersections and trim as needed.
            final long beginning = contentElement.position();
            final long ending = beginning + contentElement.size();

            for (@NotNull final PositionedDrawingBehavioralSize intersectingRegion : intersecting) {
                final long intersectingBeginning = intersectingRegion.position();
                final long intersectingEnding = intersectingBeginning + intersectingRegion.size();

                final long trimmedRegionBeginning = Math.max(beginning, intersectingBeginning);
                final long trimmedRegionEnding = Math.min(ending, intersectingEnding);

                final long trimmedRegionSize = trimmedRegionEnding - trimmedRegionBeginning;
                final DrawingBehavioralSize.Behavior trimmedRegionBehavior = intersectingRegion.behavior();

                positionedResults.add(PositionedDrawingBehavioralSize.of(new Object(), trimmedRegionBehavior, trimmedRegionBeginning, trimmedRegionSize));
            }
        }

        final List<@NotNull PositionedDrawingBehavioralSize> mergedRegions = this.mergeRegions(positionedResults);
        return this.toNormal(mergedRegions, sizeCreator);
    }

    @Contract(pure = true)
    @NotNull
    private List<@NotNull PositionedDrawingBehavioralSize> toPositioned(
            @NotNull final List<@NotNull DrawingBehavioralSize> sizes,
            @NotNull final ToLongFunction<@NotNull Size> sizeExtractor
    ) {
        final List<@NotNull PositionedDrawingBehavioralSize> positionedSizes = new ArrayList<>();

        long runningTotal = 0;
        for (@NotNull final DrawingBehavioralSize size : sizes) {
            final long length = sizeExtractor.applyAsLong(size.size());
            positionedSizes.add(PositionedDrawingBehavioralSize.of(new Object(), size.behavior(), runningTotal, length));
            runningTotal += length;
        }

        return positionedSizes;
    }

    @Contract(pure = true)
    @NotNull
    private List<@NotNull DrawingBehavioralSize> toNormal(
            @NotNull final List<@NotNull PositionedDrawingBehavioralSize> positionedSizes,
            @Range(from = 0, to = Long.MAX_VALUE) @NotNull final LongFunction<@NotNull Size> sizeCreator
    ) {
        final List<@NotNull DrawingBehavioralSize> sizes = new ArrayList<>();

        for (@NotNull final PositionedDrawingBehavioralSize positionedSize : positionedSizes) {
            sizes.add(DrawingBehavioralSize.of(sizeCreator.apply(positionedSize.size()), positionedSize.behavior()));
        }

        return sizes;
    }

    @Contract(pure = true)
    @NotNull
    private List<@NotNull PositionedDrawingBehavioralSize> intersect(
            @NotNull final List<@NotNull PositionedDrawingBehavioralSize> set,
            @NotNull final PositionedDrawingBehavioralSize element
    ) {
        final long beginning = element.position();
        final long ending = beginning + element.size();

        final List<@NotNull PositionedDrawingBehavioralSize> intersecting = new ArrayList<>();

        for (@NotNull final PositionedDrawingBehavioralSize size : set) {
            final long sizeBeginning = size.position();
            final long sizeEnding = sizeBeginning + size.size();

            // Case 1: starts within the region: (sizeBeginning <= beginning <= sizeEnding)
            // Case 2: ends within the region: (sizeBeginning <= ending <= sizeEnding)
            // Case 3: fully contained within the region: (sizeBeginning <= beginning <= ending <= sizeEnding)
            // Case 4: fully contains the region (beginning <= sizeBeginning <= sizeEnding <= ending)
            final boolean case1 = sizeBeginning <= beginning && beginning <= sizeEnding;
            final boolean case2 = sizeBeginning <= ending && ending <= sizeEnding;
            final boolean case3 = sizeBeginning <= beginning && ending <= sizeEnding;
            final boolean case4 = beginning <= sizeBeginning && sizeEnding <= ending;

            if (case1 || case2 || case3 || case4) {
                intersecting.add(size);
            }
        }

        return intersecting;
    }

    @Contract(pure = true)
    @NotNull
    private List<@NotNull PositionedDrawingBehavioralSize> mergeRegions(@NotNull final List<@NotNull PositionedDrawingBehavioralSize> sizes) {
        final List<@NotNull PositionedDrawingBehavioralSize> regions = new ArrayList<>();

        DrawingBehavioralSize.Behavior previousBehavior = null;
        PositionedDrawingBehavioralSize pending = null;
        for (@NotNull final PositionedDrawingBehavioralSize size : sizes) {
            final DrawingBehavioralSize.Behavior behavior = size.behavior();
            if (previousBehavior != behavior) {
                previousBehavior = behavior;

                if (pending != null) {
                    regions.add(pending);
                }
            }

            final Object id = pending == null? new Object() : pending.id();
            final long position = pending == null? size.position() : pending.position();
            final long pendingSize = (pending == null? 0 : pending.size()) + size.size();
            pending = PositionedDrawingBehavioralSize.of(id, behavior, position, pendingSize);
        }

        if (pending != null) {
            regions.add(pending);
        }

        return regions;
    }

    @Contract(pure = true)
    @NotNull
    private List<@NotNull DrawingGridRegion> createRegions(
            @NotNull final List<@NotNull DrawingBehavioralSize> horizontals,
            @NotNull final List<@NotNull DrawingBehavioralSize> verticals
    ) {
        final List<@NotNull DrawingGridRegion> regions = new ArrayList<>();

        long horizontalBeginning = 0;
        for (@NotNull final DrawingBehavioralSize horizontal : horizontals) {
            final long horizontalSize = horizontal.size().width();

            long verticalBeginning = 0;
            for (@NotNull final DrawingBehavioralSize vertical : verticals) {
                final long verticalSize = vertical.size().height();

                final Coordinates topLeft = Coordinates.of(horizontalBeginning, verticalBeginning);
                final Size size = Size.of(horizontalSize, verticalSize);
                final DrawingGridRegion region = DrawingGridRegion.of(topLeft, size);
                regions.add(region);

                verticalBeginning += verticalSize;
            }

            horizontalBeginning += horizontalSize;
        }

        return regions;
    }

    @Contract(pure = true)
    @NotNull
    private DrawingGrid recursivelyApplySizing(@NotNull final DrawingGrid original, @NotNull final List<@NotNull Size> contentSizes) {
        DrawingGrid grid = original;

        for (int i = 0, s = contentSizes.size(); i < s; ++i) {
            grid = this.applySizing(grid, i, contentSizes.get(i));
        }

        return grid;
    }

    @Contract(pure = true)
    @NotNull
    private DrawingGrid applySizing(
            @NotNull final DrawingGrid grid,
            @Range(from = 0, to = Integer.MAX_VALUE) final int contentIndex,
            @NotNull final Size contentSize
    ) {
        final long verticalSpots = this.withStretchBehavior(this.contentGrid.verticalSizes(), DrawingBehavioralSize::behavior).count();

        final long row = contentIndex % verticalSpots;
        final long column = contentIndex / verticalSpots;

        final List<@NotNull DrawingBehavioralSize> horizontals = this.rebuildRegions(
                column,
                this.contentGrid.horizontalSizes(),
                grid.horizontalSizes(),
                contentSize,
                Size::width,
                a -> Size.of(a, 0)
        );
        final List<@NotNull DrawingBehavioralSize> verticals = this.rebuildRegions(
                row,
                this.contentGrid.verticalSizes(),
                grid.verticalSizes(),
                contentSize,
                Size::height,
                a -> Size.of(0, a)
        );
        final List<@NotNull DrawingGridRegion> regions = this.createRegions(horizontals, verticals);
        return DrawingGrid.of(horizontals, verticals, regions);
    }

    @Contract(pure = true)
    @NotNull
    private List<@NotNull DrawingBehavioralSize> rebuildRegions(
            @Range(from = 0, to = Long.MAX_VALUE) final long index,
            @NotNull final List<@NotNull DrawingBehavioralSize> contentSizes,
            @NotNull final List<@NotNull DrawingBehavioralSize> gridSizes,
            @NotNull final Size contentSize,
            @NotNull final ToLongFunction<@NotNull Size> sizeExtractor,
            @Range(from = 0, to = Long.MAX_VALUE) @NotNull final LongFunction<@NotNull Size> sizeCreator
    ) {
        final PositionedDrawingBehavioralSize target = this.behavioralSizeNumber(index, contentSizes, sizeExtractor);
        final List<@NotNull PositionedDrawingBehavioralSize> grid = this.toPositioned(gridSizes, sizeExtractor);
        final List<@NotNull PositionedDrawingBehavioralSize> intersections = this.intersect(grid, target);
        final Set<@NotNull PositionedDrawingBehavioralSize> intersectionSet = new HashSet<>(intersections);

        // By definition, the intersecting regions are exact matches due to how we built the grid before, i.e. by
        // trimming and merging the various areas according to the content grid: we can now build a new set of regions
        // that we can then pass to the WeightedDrawingSizing to make it fit properly. Moreover, we leverage equals
        // specifically because of what we mentioned before.
        final List<@NotNull PositionedDrawingBehavioralSize> newSizes = this.refitRegions(grid, intersectionSet);
        final List<@NotNull DrawingBehavioralSize> normalizedNewSizes = this.toNormal(newSizes, sizeCreator);

        final Size resultingSize = sizeCreator.apply(this.computeSize(normalizedNewSizes, sizeExtractor, sizeExtractor.applyAsLong(contentSize)));
        final WeightedDrawingSizing sizing = WeightedDrawingSizing.of(normalizedNewSizes, resultingSize, sizeExtractor, sizeCreator);
        final List<@NotNull DrawingBehavioralSize> resizedRegions = this.regionsFromSizing(normalizedNewSizes.size(), sizing);

        final int[] intersectionPositions = this.computeIntersectionPositions(intersections, grid);
        return this.copyOnlyChangedRegions(grid, intersectionPositions, this.toPositioned(resizedRegions, sizeExtractor), sizeCreator);
    }

    @Contract(pure = true)
    @NotNull
    private PositionedDrawingBehavioralSize behavioralSizeNumber(
            @Range(from = 0, to = Long.MAX_VALUE) final long index,
            @NotNull final List<@NotNull DrawingBehavioralSize> sizes,
            @NotNull final ToLongFunction<@NotNull Size> sizeExtractor
    ) {
        return this.withStretchBehavior(this.toPositioned(sizes, sizeExtractor), PositionedDrawingBehavioralSize::behavior)
                .skip(index)
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("Found index that does not exist in regions: this makes no sense"));
    }

    @Contract(pure = true)
    @Range(from = 0, to = Long.MAX_VALUE)
    private <T> Stream<@NotNull T> withStretchBehavior(
            @NotNull final List<@NotNull T> sizes,
            @NotNull final Function<@NotNull T, DrawingBehavioralSize. @NotNull Behavior> behaviorGetter
    ) {
        return sizes.stream().filter(it -> behaviorGetter.apply(it) == DrawingBehavioralSize.Behavior.STRETCH);
    }

    @Contract(pure = true)
    @NotNull
    private List<@NotNull PositionedDrawingBehavioralSize> refitRegions(
            @NotNull final List<@NotNull PositionedDrawingBehavioralSize> grid,
            @NotNull final Set<@NotNull PositionedDrawingBehavioralSize> intersecting
    ) {
        final List<@NotNull PositionedDrawingBehavioralSize> newGrid = new ArrayList<>();

        for (@NotNull final PositionedDrawingBehavioralSize size : grid) {
            if (intersecting.contains(size)) {
                newGrid.add(size);
                continue;
            }

            newGrid.add(PositionedDrawingBehavioralSize.of(size.id(), DrawingBehavioralSize.Behavior.FIX, size.position(), size.size()));
        }

        return newGrid;
    }

    @Contract(pure = true)
    @NotNull
    private List<@NotNull DrawingBehavioralSize> regionsFromSizing(
            @Range(from = 0, to = Integer.MAX_VALUE) final int count,
            @NotNull final WeightedDrawingSizing sizing
    ) {
        final List<@NotNull DrawingBehavioralSize> sizes = new ArrayList<>(count);
        for (int i = 0; i < count; ++i) {
            final Size regionSize = sizing.sizeRegion(i);
            sizes.add(DrawingBehavioralSize.of(regionSize, DrawingBehavioralSize.Behavior.FIX));
        }
        return sizes;
    }

    @Contract(pure = true)
    @Range(from = 0, to = Integer.MAX_VALUE)
    private int @NotNull [] computeIntersectionPositions(
            @NotNull final List<@NotNull PositionedDrawingBehavioralSize> intersections,
            @NotNull final List<@NotNull PositionedDrawingBehavioralSize> grid
    ) {
        final int intersectionsCount = intersections.size();
        final int[] positions = new int[intersectionsCount];

        for (int i = 0; i < intersectionsCount; ++i) {
            positions[i] = grid.indexOf(intersections.get(i));
            assert positions[i] != -1;
        }

        return positions;
    }

    @Contract(pure = true)
    private List<@NotNull DrawingBehavioralSize> copyOnlyChangedRegions(
            @NotNull final List<@NotNull PositionedDrawingBehavioralSize> grid,
            @Range(from = 0, to = Integer.MAX_VALUE) int @NotNull [] positions,
            @NotNull final List<@NotNull PositionedDrawingBehavioralSize> resizedRegions,
            @Range(from = 0, to = Long.MAX_VALUE) @NotNull final LongFunction<@NotNull Size> sizeCreator
    ) {
        final int[] binarySorted = Arrays.copyOf(positions, positions.length);
        Arrays.sort(binarySorted);

        assert grid.size() == resizedRegions.size();

        final List<@NotNull PositionedDrawingBehavioralSize> finalRegionSet = new ArrayList<>();

        for (int i = 0, s = grid.size(); i < s; ++i) {
            final boolean requiresCopy = Arrays.binarySearch(binarySorted, i) != -1;
            final PositionedDrawingBehavioralSize original = grid.get(i);
            final PositionedDrawingBehavioralSize resized = resizedRegions.get(i);

            final DrawingBehavioralSize.Behavior behavior = requiresCopy? resized.behavior() : original.behavior();
            final Object id = requiresCopy? new Object() : original.id();
            final long position = original.position();
            final long size = original.size();

            final PositionedDrawingBehavioralSize region = PositionedDrawingBehavioralSize.of(id, behavior, position, size);
            finalRegionSet.add(region);
        }

        return this.toNormal(finalRegionSet, sizeCreator);
    }

    @Contract(pure = true)
    @NotNull
    private Size computeSize(
            @NotNull final DrawingGrid grid,
            @Range(from = 0, to = Long.MAX_VALUE) @SuppressWarnings("SameParameterValue") final long sizeForVerticalStretch,
            @Range(from = 0, to = Long.MAX_VALUE) @SuppressWarnings("SameParameterValue") final long sizeForHorizontalStretch
    ) {
        final long width = this.computeSize(grid.horizontalSizes(), Size::width, sizeForHorizontalStretch);
        final long height = this.computeSize(grid.verticalSizes(), Size::height, sizeForVerticalStretch);
        return Size.of(width, height);
    }

    @Contract(pure = true)
    @Range(from = 0, to = Long.MAX_VALUE)
    private long computeSize(
            @NotNull final List<@NotNull DrawingBehavioralSize> size,
            @NotNull final ToLongFunction<@NotNull Size> sizeExtractor,
            @Range(from = 0, to = Long.MAX_VALUE) final long stretchSize
    ) {
        return size.stream()
                .mapToLong(it -> it.behavior() == DrawingBehavioralSize.Behavior.STRETCH? stretchSize : sizeExtractor.applyAsLong(it.size()))
                .sum();
    }

}
