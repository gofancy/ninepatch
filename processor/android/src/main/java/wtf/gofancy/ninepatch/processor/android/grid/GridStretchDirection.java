package wtf.gofancy.ninepatch.processor.android.grid;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Range;

import java.util.Objects;
import java.util.function.Function;
import java.util.function.ToIntFunction;

enum GridStretchDirection {
    HORIZONTAL,
    VERTICAL,
    BOTH,
    NONE;

    @NotNull private static final GridStretchDirection @NotNull [] FOR_STRETCH_COLUMN = { HORIZONTAL, BOTH };
    @NotNull private static final GridStretchDirection @NotNull [] FOR_FIX_COLUMN = { NONE, VERTICAL };

    @Contract("_, _, _ -> new")
    @NotNull
    static <D> Function<@NotNull D, @NotNull GridStretchDirection> stretchFix(
            @NotNull final NinePatchColumnRegion.ColumnKind kind,
            @NotNull final D stretchColor,
            @NotNull final D fixColor
    ) {
        Objects.requireNonNull(kind, "kind");
        Objects.requireNonNull(stretchColor, "stretchColor");
        Objects.requireNonNull(fixColor, "fixColor");

        final GridStretchDirection @NotNull [] targetArray = kind == NinePatchColumnRegion.ColumnKind.STRETCH? FOR_STRETCH_COLUMN : FOR_FIX_COLUMN;
        final ToIntFunction<@NotNull D> selector = it -> Objects.equals(it, stretchColor)? 1 : Objects.equals(it, fixColor)? 0 : -1;
        return it -> targetArray[requirePositive(selector.applyAsInt(it))];
    }

    @Contract(value = "_ -> param1", pure = true)
    @Range(from = 0, to = 1)
    private static int requirePositive(@Range(from = -1, to = 1) final int id) {
        if (id < 0) throw new IllegalArgumentException("Invalid color: only fix or stretch colors supported");
        return id;
    }
}
