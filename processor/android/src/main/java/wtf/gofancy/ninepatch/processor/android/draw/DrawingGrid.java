package wtf.gofancy.ninepatch.processor.android.draw;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Unmodifiable;
import wtf.gofancy.ninepatch.processor.android.grid.NinePatchGrid;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

public final class DrawingGrid {
    @NotNull @Unmodifiable private final List<@NotNull DrawingBehavioralSize> columnSizes;
    @NotNull @Unmodifiable private final List<@NotNull DrawingBehavioralSize> rowSizes;
    @NotNull @Unmodifiable private final List<@NotNull DrawingGridRegion> regions;

    private DrawingGrid(
            @NotNull final List<@NotNull DrawingBehavioralSize> columnSizes,
            @NotNull final List<@NotNull DrawingBehavioralSize> rowSizes,
            @NotNull final List<@NotNull DrawingGridRegion> regions
    ) {
        this.columnSizes = Collections.unmodifiableList(columnSizes);
        this.rowSizes = Collections.unmodifiableList(rowSizes);
        this.regions = Collections.unmodifiableList(regions);
    }

    @Contract("_ -> new")
    @NotNull
    public static DrawingGrid from(@NotNull final NinePatchGrid grid) {
        Objects.requireNonNull(grid, "grid");
        return of(
                convert(grid.horizontalSizes(), DrawingBehavioralSize::from),
                convert(grid.verticalSizes(), DrawingBehavioralSize::from),
                convert(grid.regions(), DrawingGridRegion::from)
        );
    }

    @Contract("_, _, _ -> new")
    @NotNull
    static DrawingGrid of(
            @NotNull final List<@NotNull DrawingBehavioralSize> columnSizes,
            @NotNull final List<@NotNull DrawingBehavioralSize> rowSizes,
            @NotNull final List<@NotNull DrawingGridRegion> regions
    ) {
        return new DrawingGrid(
                Objects.requireNonNull(columnSizes, "columnSizes"),
                Objects.requireNonNull(rowSizes, "rowSizes"),
                Objects.requireNonNull(regions, "regions")
        );
    }

    @Contract("_, _ -> _")
    @NotNull
    private static <T, U> List<@NotNull T> convert(@NotNull final List<@NotNull U> list, @NotNull final Function<@NotNull U, @NotNull T> converter) {
        return list.stream().map(converter).collect(Collectors.toList());
    }

    @Contract(pure = true)
    @NotNull
    @Unmodifiable
    public List<@NotNull DrawingBehavioralSize> horizontalSizes() {
        return this.columnSizes;
    }

    @Contract(pure = true)
    @NotNull
    @Unmodifiable
    public List<@NotNull DrawingBehavioralSize> verticalSizes() {
        return this.rowSizes;
    }

    @Contract(pure = true)
    @NotNull
    @Unmodifiable
    public List<@NotNull DrawingGridRegion> regions() {
        return this.regions;
    }
}
