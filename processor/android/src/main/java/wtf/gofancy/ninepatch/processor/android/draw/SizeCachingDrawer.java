package wtf.gofancy.ninepatch.processor.android.draw;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.Range;
import wtf.gofancy.ninepatch.core.data.Coordinates;
import wtf.gofancy.ninepatch.core.data.ImageData;
import wtf.gofancy.ninepatch.core.data.Size;
import wtf.gofancy.ninepatch.core.io.Surface;
import wtf.gofancy.ninepatch.processor.android.grid.AndroidGrids;

import java.util.List;
import java.util.Objects;
import java.util.function.BiPredicate;
import java.util.function.Function;

public final class SizeCachingDrawer<T> {
    public static final class CacheComputationData {
        @NotNull private final DrawingGrid grid;
        @NotNull private final Size desiredSize;

        private CacheComputationData(@NotNull final DrawingGrid grid, @NotNull final Size desiredSize) {
            this.grid = grid;
            this.desiredSize = desiredSize;
        }

        @Contract("_, _ -> new")
        @NotNull
        public static SizeCachingDrawer.CacheComputationData of(@NotNull final DrawingGrid grid, @NotNull final Size desiredSize) {
            return new CacheComputationData(
                    Objects.requireNonNull(grid, "grid"),
                    Objects.requireNonNull(desiredSize, "desiredSize")
            );
        }

        @Contract(pure = true)
        @NotNull
        DrawingGrid grid() {
            return this.grid;
        }

        @Contract(pure = true)
        @NotNull
        Size desiredSize() {
            return this.desiredSize;
        }
    }

    @SuppressWarnings("ClassCanBeRecord") // Java
    private static final class CachedData {
        @NotNull private final List<@NotNull DrawingGridRegion> regions;
        @NotNull private final WeightedDrawingSizing horizontalSizing;
        @NotNull private final WeightedDrawingSizing verticalSizing;
        @Range(from = 1, to = Long.MAX_VALUE) private final int column;

        private CachedData(
                @NotNull final List<@NotNull DrawingGridRegion> regions,
                @NotNull final WeightedDrawingSizing horizontalSizing,
                @NotNull final WeightedDrawingSizing verticalSizing,
                @Range(from = 1, to = Long.MAX_VALUE) final int column
        ) {
            this.regions = regions;
            this.horizontalSizing = horizontalSizing;
            this.verticalSizing = verticalSizing;
            this.column = column;
        }

        @Contract("_, _, _, _ -> new")
        @NotNull
        static CachedData of(
                @NotNull final List<@NotNull DrawingGridRegion> regions,
                @NotNull final WeightedDrawingSizing horizontalSizing,
                @NotNull final WeightedDrawingSizing verticalSizing,
                @Range(from = 1, to = Long.MAX_VALUE) final int column
        ) {
            return new CachedData(regions, horizontalSizing, verticalSizing, column);
        }

        @Contract(pure = true)
        @NotNull
        List<@NotNull DrawingGridRegion> regions() {
            return this.regions;
        }

        @Contract(pure = true)
        @NotNull
        WeightedDrawingSizing horizontalSizing() {
            return this.horizontalSizing;
        }

        @Contract(pure = true)
        @NotNull
        WeightedDrawingSizing verticalSizing() {
            return this.verticalSizing;
        }

        @Contract(pure = true)
        @Range(from = 1, to = Integer.MAX_VALUE)
        int columns() {
            return this.column;
        }
    }

    @NotNull private final AndroidGrids grids;
    @NotNull private final BiPredicate<@Nullable T, @NotNull T> equalityChecker;

    @Nullable private T cacheKey;
    @Nullable private CachedData data;

    private SizeCachingDrawer(@NotNull final AndroidGrids grids, @NotNull final BiPredicate<@Nullable T, @NotNull T> equalityChecker) {
        this.grids = grids;
        this.equalityChecker = equalityChecker;
        this.cacheKey = null;
        this.data = null;
    }

    @Contract("_, _ -> new")
    @NotNull
    public static <T> SizeCachingDrawer<T> of(@NotNull final AndroidGrids grids, @NotNull final BiPredicate<@Nullable T, @NotNull T> equalityChecker) {
        return new SizeCachingDrawer<>(
                Objects.requireNonNull(grids, "grids"),
                Objects.requireNonNull(equalityChecker, "equalityChecker")
        );
    }

    @Contract("_, _, _, _ -> _")
    public <I, D> void drawWithCache(
            @NotNull final Surface<I, D> surface,
            @NotNull final ImageData<I, D> data,
            @NotNull final T cacheKey,
            @NotNull final Function<@NotNull AndroidGrids, @NotNull CacheComputationData> gridComputer
    ) {
        Objects.requireNonNull(surface, "surface");
        Objects.requireNonNull(cacheKey, "cacheKey");
        Objects.requireNonNull(gridComputer, "gridComputer");

        if (!this.equalityChecker.test(this.cacheKey, cacheKey)) {
            this.cacheKey = cacheKey;
            this.data = this.computeCachedData(gridComputer.apply(this.grids));
        }

        assert this.data != null;
        this.drawWithCache(surface, data, this.data);
    }

    @Contract("_ -> new")
    @NotNull
    private CachedData computeCachedData(@NotNull final CacheComputationData data) {
        final DrawingGrid grid = data.grid();
        final Size desiredSize = data.desiredSize();

        final WeightedDrawingSizing horizontalSizing = WeightedDrawingSizing.of(grid.horizontalSizes(), desiredSize, Size::width, a -> Size.of(a, 0));
        final WeightedDrawingSizing verticalSizing = WeightedDrawingSizing.of(grid.verticalSizes(), desiredSize, Size::height, a -> Size.of(0, a));
        final List<@NotNull DrawingGridRegion> regions = grid.regions();
        final int columns = grid.horizontalSizes().size();

        return CachedData.of(regions, horizontalSizing, verticalSizing, columns);
    }

    @Contract("_, _, _ -> _")
    private <I, D> void drawWithCache(
            @NotNull final Surface<I, D> surface,
            @NotNull final ImageData<I, D> data,
            @NotNull final CachedData cachedData
    ) {
        this.drawRegionsOnSurface(
                surface,
                data,
                cachedData.regions(),
                cachedData.horizontalSizing(),
                cachedData.verticalSizing(),
                cachedData.columns()
        );
    }

    @Contract("_, _, _, _, _, _ -> _")
    private <I, D> void drawRegionsOnSurface(
            @NotNull final Surface<I, D> surface,
            @NotNull final ImageData<I, D> data,
            @NotNull final List<@NotNull DrawingGridRegion> regions,
            @NotNull final WeightedDrawingSizing horizontalSizing,
            @NotNull final WeightedDrawingSizing verticalSizing,
            @Range(from = 1, to = Long.MAX_VALUE) final int columns
    ) {
        boolean updateColumn = false;
        long columnSizeRollingTotal = 0;
        long rowSizeRollingTotal = 0;
        for (int i = 0, s = regions.size(); i < s; ++i) {
            final DrawingGridRegion region = regions.get(i);
            final int column = i / columns;
            final int row = i % columns;

            if (row == 0) {
                rowSizeRollingTotal = 0;
                updateColumn = true;
            }

            final Size columnSizing = horizontalSizing.sizeRegion(column);
            final Size rowSizing = verticalSizing.sizeRegion(row);
            final Size size = Size.of(columnSizing.width(), rowSizing.height());

            final Coordinates coordinates = Coordinates.of(columnSizeRollingTotal, rowSizeRollingTotal);

            surface.drawSection(
                    data,
                    region.topLeftCoordinates(),
                    region.regionSizeOnImage(),
                    coordinates,
                    size
            );

            rowSizeRollingTotal += size.height();
            if (updateColumn) {
                columnSizeRollingTotal += size.width();
                updateColumn = false;
            }
        }
    }
}
