package wtf.gofancy.ninepatch.processor.android.marking;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Range;

public final class ColoredMarkingSection<D> {
    @NotNull
    private final MarkingSection section;
    @NotNull private final D color;

    private ColoredMarkingSection(@NotNull final MarkingSection section, @NotNull final D color) {
        this.section = section;
        this.color = color;
    }

    @Contract("_, _ -> new")
    @NotNull
    static <D> ColoredMarkingSection<D> of(@NotNull final MarkingSection section, @NotNull final D color) {
        return new ColoredMarkingSection<D>(section, color);
    }

    @Contract(pure = true)
    @Range(from = 0, to = Long.MAX_VALUE)
    public long displacement() {
        return this.section.displacement();
    }

    @Contract(pure = true)
    @Range(from = 0, to = Long.MAX_VALUE)
    public long size() {
        return this.section.size();
    }

    @Contract(pure = true)
    @NotNull
    public D color() {
        return this.color;
    }
}
