package wtf.gofancy.ninepatch.processor.android.marking;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.UnmodifiableView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

final class MarkingBuffer {
    @NotNull private final List<MarkingSection> sections;
    @NotNull private final MarkingSectionDirection direction;

    private boolean ended;
    private long beginAt;
    private long accumulatingSize;

    private MarkingBuffer(@NotNull final MarkingSectionDirection direction) {
        this.sections = new ArrayList<>();
        this.direction = direction;
        this.ended = false;
        this.beginAt = 0;
        this.accumulatingSize = 0;
    }

    @Contract("_ -> new")
    @NotNull
    static MarkingBuffer directional(@NotNull final MarkingSectionDirection direction) {
        return new MarkingBuffer(Objects.requireNonNull(direction, "direction"));
    }

    @Contract("-> _")
    void add() {
        this.open();
        this.accumulatingSize += 1;
    }

    @Contract("-> _")
    void next() {
        this.open();
        final MarkingSection section = MarkingSection.of(this.direction, this.beginAt, this.accumulatingSize);
        this.sections.add(section);
        this.beginAt += this.accumulatingSize;
        this.accumulatingSize = 0;
    }

    @Contract("-> _")
    @NotNull
    @UnmodifiableView
    List<MarkingSection> end() {
        this.open();
        this.ended = true;
        return Collections.unmodifiableList(this.sections);
    }

    @Contract(pure = true)
    private void open() {
        if (this.ended) {
            throw new IllegalStateException("Marking Buffer has been terminated");
        }
    }
}
