package wtf.gofancy.ninepatch.processor.android;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Range;
import wtf.gofancy.ninepatch.core.NinePatchImage;
import wtf.gofancy.ninepatch.core.data.ImageData;
import wtf.gofancy.ninepatch.core.data.Size;
import wtf.gofancy.ninepatch.core.io.Surface;
import wtf.gofancy.ninepatch.processor.android.draw.*;
import wtf.gofancy.ninepatch.processor.android.grid.NinePatchGrid;

import java.util.List;
import java.util.Objects;

final class AndroidNinePatchImage<I, D> implements NinePatchImage<I, D> {
    @NotNull private final ImageData<I, D> data;
    @NotNull private final SizeCachingDrawer<@NotNull Size> sizeFittingCache;
    @NotNull private final SizeCachingDrawer<@NotNull List<@NotNull Size>> contentFittingCache;
    @Range(from = 1, to = Long.MAX_VALUE) private final long contentSlots;

    private AndroidNinePatchImage(
            @NotNull final ImageData<I, D> data,
            @NotNull final SizeCachingDrawer<@NotNull Size> sizeFittingCache,
            @NotNull final SizeCachingDrawer<@NotNull List<@NotNull Size>> contentFittingCache,
            @Range(from = 1, to = Long.MAX_VALUE) final long contentSlots
    ) {
        this.data = data;
        this.sizeFittingCache = sizeFittingCache;
        this.contentFittingCache = contentFittingCache;
        this.contentSlots = contentSlots;
    }

    @Contract("_, _, _, _ -> new")
    @NotNull
    static <I, D> NinePatchImage<I, D> of(
            @NotNull final ImageData<I, D> data,
            @NotNull final SizeCachingDrawer<@NotNull Size> sizeFittingCache,
            @NotNull final SizeCachingDrawer<@NotNull List<@NotNull Size>> contentFittingCache,
            @Range(from = 1, to = Long.MAX_VALUE) final long contentSlots
    ) {
        return new AndroidNinePatchImage<>(
                Objects.requireNonNull(data, "data"),
                Objects.requireNonNull(sizeFittingCache, "sizeFittingCache"),
                Objects.requireNonNull(contentFittingCache, "contentFittingCache"),
                requirePositive(contentSlots)
        );
    }

    @Contract(pure = true, value = "_ -> param1")
    @Range(from = 1, to = Long.MAX_VALUE)
    private static long requirePositive(final long slots) {
        if (slots < 1) {
            throw new IllegalArgumentException("No content slots found: this is a critical error");
        }
        return slots;
    }

    @Contract(pure = true)
    @Override
    @NotNull
    public ImageData<I, D> data() {
        return this.data;
    }

    @Override
    public void drawFittingSize(@NotNull final Surface<I, D> surface, @NotNull final Size desiredSize) {
        Objects.requireNonNull(surface, "surface");
        Objects.requireNonNull(desiredSize, "desiredSize");

        this.sizeFittingCache.drawWithCache(surface, this.data(), desiredSize, grids -> {
            final NinePatchGrid stretchingGrid = grids.stretchingGrid();
            final DrawingGrid drawingGrid = DrawingGrid.from(stretchingGrid);
            return SizeCachingDrawer.CacheComputationData.of(drawingGrid, desiredSize);
        });
    }

    @Override
    public void drawFittingContents(@NotNull final Surface<I, D> surface, @NotNull final List<Size> desiredContentSizes) {
        Objects.requireNonNull(surface, "surface");
        Objects.requireNonNull(desiredContentSizes, "desiredContentSizes");
        if (desiredContentSizes.size() != this.contentSlots) {
            throw new IllegalArgumentException("Expected " + this.contentSlots + " but found only " + desiredContentSizes.size());
        }

        this.contentFittingCache.drawWithCache(surface, this.data(), desiredContentSizes, grids -> {
            final DrawingGrid stretchingGrid = DrawingGrid.from(grids.stretchingGrid());
            final DrawingGrid contentGrid = DrawingGrid.from(grids.contentPositioning());
            final MergedGridComputer computer = MergedGridComputer.of(stretchingGrid, contentGrid);

            final DrawingGrid resultingGrid = computer.buildGrid(desiredContentSizes);
            final Size desiredSize = computer.computeSize(resultingGrid);
            return SizeCachingDrawer.CacheComputationData.of(resultingGrid, desiredSize);
        });
    }
}
