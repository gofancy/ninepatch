package wtf.gofancy.ninepatch.processor.android;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.Range;
import wtf.gofancy.ninepatch.core.NinePatchImage;
import wtf.gofancy.ninepatch.core.data.Size;
import wtf.gofancy.ninepatch.core.io.ImageSource;
import wtf.gofancy.ninepatch.core.processor.spi.ImageProcessor;
import wtf.gofancy.ninepatch.processor.android.draw.SizeCachingDrawer;
import wtf.gofancy.ninepatch.processor.android.grid.AndroidGrids;
import wtf.gofancy.ninepatch.processor.android.marking.ColoredMarkingSection;
import wtf.gofancy.ninepatch.processor.android.marking.MeaningfulMarkings;
import wtf.gofancy.ninepatch.processor.android.marking.SidedMarkings;

import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;

public final class AndroidProcessor implements ImageProcessor.SubVersionIgnoringImageProcessor {
    private AndroidProcessor() {}

    @Contract(value = "-> new", pure = true)
    @NotNull
    public static ImageProcessor provider() {
        return new AndroidProcessor();
    }

    @Override
    public int rgba32VersionTag() {
        return 0xFFFFFF00;
    }

    @Contract("_ -> new")
    @NotNull
    @Override
    public <I, D> NinePatchImage<I, D> process(@NotNull final ImageSource<I, D> source) {
        final MeaningfulMarkings<D> markings = this.readMarkings(source);
        final MeaningfulMarkings<D> validatedMarkings = this.validateMarkings(markings);
        final long contentSlots = this.computeContentSlots(validatedMarkings);
        final AndroidGrids grids = this.constructGrids(source, validatedMarkings);
        final SizeCachingDrawer<@NotNull Size> sizeCaching = SizeCachingDrawer.of(grids, Objects::equals);
        final SizeCachingDrawer<@NotNull List<@NotNull Size>> listCaching = SizeCachingDrawer.of(grids, Objects::equals);
        return AndroidNinePatchImage.of(source.data(), sizeCaching, listCaching, contentSlots);
    }

    @Contract("_ -> new")
    @NotNull
    private <I, D> MeaningfulMarkings<D> readMarkings(@NotNull final ImageSource<I, D> source) {
        final SidedMarkings<D> markings = SidedMarkings.readSidedMarkings(source);
        return MeaningfulMarkings.from(markings);
    }

    @Contract("_ -> new")
    @NotNull
    private <D> MeaningfulMarkings<D> validateMarkings(@NotNull final MeaningfulMarkings<D> markings) {
        final List<@NotNull ColoredMarkingSection<D>> horizontalContent = markings.horizontalContentArea();
        final List<@NotNull ColoredMarkingSection<D>> verticalContent = markings.verticalContentArea();

        final List<@NotNull ColoredMarkingSection<D>> validatedHorizontal = this.validateContentSection(horizontalContent, markings::horizontalStretch);
        final List<@NotNull ColoredMarkingSection<D>> validatedVertical = this.validateContentSection(verticalContent, markings::verticalStretch);

        return MeaningfulMarkings.validated(
                markings,
                null,
                null,
                validatedHorizontal,
                validatedVertical
        );
    }

    @Contract("_, _ -> _")
    @Nullable
    private <D> List<@NotNull ColoredMarkingSection<D>> validateContentSection(
            @NotNull final List<@NotNull ColoredMarkingSection<D>> contentSections,
            @NotNull final Supplier<@NotNull List<@NotNull ColoredMarkingSection<D>>> correspondingStretchSections
    ) {
        if (contentSections.size() <= 1) { // Should never be empty, but let's code defensively
            return correspondingStretchSections.get();
        }
        return null;
    }

    @Contract("_ -> _")
    @Range(from = 1, to = Long.MAX_VALUE)
    private <D> long computeContentSlots(@NotNull final MeaningfulMarkings<D> markings) {
        final List<@NotNull ColoredMarkingSection<D>> horizontalSections = markings.horizontalContentArea();
        final List<@NotNull ColoredMarkingSection<D>> verticalSections = markings.verticalContentArea();

        return (horizontalSections.size() / 2L) * (verticalSections.size() / 2L);
    }

    @Contract("_, _, -> _")
    @NotNull
    private <I, D> AndroidGrids constructGrids(@NotNull final ImageSource<I, D> source, @NotNull final MeaningfulMarkings<D> markings) {
        return AndroidGrids.fromMarkings(markings, source);
    }
}

