package wtf.gofancy.ninepatch.processor.android.grid;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import wtf.gofancy.ninepatch.core.data.Coordinates;
import wtf.gofancy.ninepatch.core.data.Size;
import wtf.gofancy.ninepatch.processor.android.marking.ColoredMarkingSection;

import java.util.Objects;
import java.util.function.Function;

final class NinePatchColumnRegion {
    enum ColumnKind {
        STRETCH,
        FIX;

        @Contract(pure = true)
        @NotNull
        static <D> Function<@NotNull D, @NotNull ColumnKind> stretchFix(@NotNull final D stretch, @NotNull final D fix) {
            Objects.requireNonNull(stretch, "stretch");
            Objects.requireNonNull(fix, "fix");
            final Function<@NotNull D, @Nullable ColumnKind> lookup = it -> Objects.equals(it, stretch)? STRETCH : Objects.equals(it, fix)? FIX : null;
            return it -> Objects.requireNonNull(lookup.apply(it), "Invalid color given: neither stretch nor fix");
        }
    }

    @NotNull private final Coordinates topLeft;
    @NotNull private final Size size;
    @NotNull private final ColumnKind kind;

    private NinePatchColumnRegion(@NotNull final Coordinates topLeft, @NotNull final Size size, @NotNull final ColumnKind kind) {
        this.topLeft = topLeft;
        this.size = size;
        this.kind = kind;
    }

    @Contract("_, _, _, _ -> new")
    @NotNull
    static <D> NinePatchColumnRegion of(
            @NotNull final ColoredMarkingSection<D> section,
            @NotNull final D stretch,
            @NotNull final D fix,
            @NotNull final Size size
    ) {
        return of(section, ColumnKind.stretchFix(stretch, fix), size);
    }

    @Contract("_, _, _ -> new")
    @NotNull
    private static <D> NinePatchColumnRegion of(
            @NotNull final ColoredMarkingSection<D> section,
            @NotNull final Function<@NotNull D, @NotNull ColumnKind> lookup,
            @NotNull final Size imageSize
    ) {
        final ColumnKind kind = lookup.apply(section.color());
        final Coordinates coordinates = Coordinates.of(section.displacement(), 1);
        final Size size = Size.of(section.size(), imageSize.height() - 2);
        return new NinePatchColumnRegion(coordinates, size, kind);
    }

    @Contract(pure = true)
    @NotNull
    ColumnKind kind() {
        return this.kind;
    }

    @Contract(pure = true)
    @NotNull
    Coordinates topLeftCoordinates() {
        return this.topLeft;
    }

    @Contract(pure = true)
    @NotNull
    Size size() {
        return this.size;
    }

}
