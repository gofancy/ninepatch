package wtf.gofancy.ninepatch.processor.android.marking;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.Unmodifiable;
import wtf.gofancy.ninepatch.core.data.ImagePixelDataFormat;
import wtf.gofancy.ninepatch.core.processor.ProcessorUtilities;

import java.util.*;

final class ColorAlternatingMarkingBuffer<D> {
    @NotNull private final MarkingBuffer delegate;
    @NotNull private final ImagePixelDataFormat<D> format;
    @NotNull private final D firstColor;
    @NotNull private final D secondColor;

    @Nullable private D firstSeenColor;
    @Nullable private D currentColor;
    @Nullable private List<MarkingSection> sections;

    private ColorAlternatingMarkingBuffer(
            @NotNull final MarkingSectionDirection direction,
            @NotNull final ImagePixelDataFormat<D> format,
            @NotNull final D firstColor,
            @NotNull final D secondColor
    ) {
        this.delegate = MarkingBuffer.directional(direction);
        this.format = format;
        this.firstColor = firstColor;
        this.secondColor = secondColor;
        this.firstSeenColor = null;
        this.currentColor = null;
        this.sections = null;
    }

    @Contract("_, _, _ , _-> new")
    @NotNull
    static <D> ColorAlternatingMarkingBuffer<D> directional(
            @NotNull final MarkingSectionDirection direction,
            @NotNull final ImagePixelDataFormat<D> format,
            @NotNull final D firstColor,
            @NotNull final D secondColor
    ) {
        if (Objects.equals(Objects.requireNonNull(firstColor, "firstColor"), Objects.requireNonNull(secondColor, "secondColor"))) {
            throw new IllegalArgumentException("Unable to use the same color as both first and second");
        }
        return new ColorAlternatingMarkingBuffer<>(
                Objects.requireNonNull(direction, "direction"),
                Objects.requireNonNull(format, "format"),
                firstColor,
                secondColor
        );
    }

    @Contract(pure = true)
    @NotNull
    D initialColor() {
        return Objects.requireNonNull(this.firstSeenColor, "Unable to query initial color before submitting data to buffer");
    }

    @Contract("_ -> _")
    void then(@NotNull final D color) {
        Objects.requireNonNull(color, "color");

        final boolean isFirst = Objects.equals(this.firstColor, color);
        final boolean isSecond = !isFirst && this.isSecond(color);

        if (!isFirst && !isSecond) {
            throw new IllegalArgumentException("Invalid color found for an alternative marking buffer: neither first nor second");
        }

        final D normalizedColor = isFirst? this.firstColor : this.secondColor;

        if (this.firstSeenColor == null) {
            this.firstSeenColor = this.currentColor = normalizedColor;
        }
        if (this.currentColor != normalizedColor) {
            this.delegate.next();
            this.currentColor = normalizedColor;
        }

        this.delegate.add();
    }

    @Contract("-> _")
    @NotNull
    @Unmodifiable
    List<@NotNull ColoredMarkingSection<D>> sections() {
        if (this.sections == null) {
            this.sections = this.delegate.end();
        }

        final D firstColor = this.initialColor();
        final D secondColor = this.firstColor == firstColor? this.secondColor : this.firstColor;

        assert this.sections != null;

        final int sectionSize = this.sections.size();
        final List<ColoredMarkingSection<D>> coloredSections = new ArrayList<>(sectionSize);

        Collections.fill(coloredSections, null);

        for (int i = 0; i < sectionSize; ++i) {
            final D color = i % 2 == 0? firstColor : secondColor;
            final ColoredMarkingSection<D> coloredSection = ColoredMarkingSection.of(this.sections.get(i), color);
            coloredSections.set(i, coloredSection);
        }

        assert !coloredSections.contains(null);

        return Collections.unmodifiableList(coloredSections);
    }

    private boolean isSecond(@NotNull final D color) {
        if (Objects.equals(this.secondColor, color)) {
            return true;
        }
        final int rgba32Color = this.format.toRgba32(color);
        return ProcessorUtilities.isTransparent(rgba32Color);
    }
}
