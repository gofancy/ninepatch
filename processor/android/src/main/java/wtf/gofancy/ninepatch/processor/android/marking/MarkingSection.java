package wtf.gofancy.ninepatch.processor.android.marking;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Range;

import java.util.Objects;

final class MarkingSection {
    @NotNull private final MarkingSectionDirection direction;
    @Range(from = 0, to = Long.MAX_VALUE) private final long displacement;
    @Range(from = 0, to = Long.MAX_VALUE) private final long size;

    private MarkingSection(
            @NotNull final MarkingSectionDirection direction,
            @Range(from = 0, to = Long.MAX_VALUE) final long displacement,
            @Range(from = 0, to = Long.MAX_VALUE) final long size
    ) {
        this.direction = direction;
        this.displacement = displacement;
        this.size = size;
    }

    @Contract("_, _, _ -> new")
    @NotNull
    static MarkingSection of(
            @NotNull final MarkingSectionDirection direction,
            @Range(from = 0, to = Long.MAX_VALUE) final long displacement,
            @Range(from = 0, to = Long.MAX_VALUE) final long size
    ) {
        return new MarkingSection(Objects.requireNonNull(direction, "direction"), displacement, size);
    }

    @Contract(pure = true)
    @NotNull
    MarkingSectionDirection direction() {
        return this.direction;
    }

    @Contract(pure = true)
    @Range(from = 0, to = Long.MAX_VALUE)
    long displacement() {
        return this.displacement;
    }

    @Contract(pure = true)
    @Range(from = 0, to = Long.MAX_VALUE)
    long size() {
        return this.size;
    }
}
