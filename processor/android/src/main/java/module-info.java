module wtf.gofancy.ninepatch.processor.android {
    requires static org.jetbrains.annotations;

    requires wtf.gofancy.ninepatch.core;

    provides wtf.gofancy.ninepatch.core.processor.spi.ImageProcessor with wtf.gofancy.ninepatch.processor.android.AndroidProcessor;
}
