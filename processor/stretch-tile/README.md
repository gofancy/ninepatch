# Stretch/Tile Format for NinePatch
The Stretch/Tile format identifies two types of non-overlapping areas: stretch areas and tile areas. A stretch area is
an area that will be stretched in order to fit the given size; a tile area identifies an area that will be tiled (i.e.
repeated) in order to cover the given area.

## Specifications Data
| Name                   | Value                   |
|------------------------|-------------------------|
| Version Tag            | `0xffff00ff`            |
| Sub-versions           | See Sub-version details |
| Content-sizing Support | No                      |
| Surface-sizing Support | Yes                     |

## Sub-versions
The sub-versions are used to encode additional metadata. In particular, given the RGBA32 format specified by
`0xrrggbbaa`, the following decisions are taken:

- If the value of `aa` is anything other than `ff`, then the subversion is considered the default `0xffffff00`. In this
  case, tiling behavior is left up to the implementation;
- Otherwise, the `rr` byte is considered as a set of flags, according to the format `0bABCDEFGH`, with the
  following meaning:
  - `AB` is used to determine the horizontal alignment preference for tiling, with the following values:
    - `00` means no preference: the implementation is free to decide the horizontal alignment as it sees fits;
    - `01` means aligning to the right: the left edge will be cut-off if needed
    - `10` means aligning to the left: the right edge will be cut-off if needed
    - `11` means aligning to the center: the left and right edge will be cut-off equally if needed
  - `CD` is used to determine the vertical alignment preference for tiling, with the following values:
    - `00` means no preference: the implementation is free to decide the vertical alignment as it sees fits;
    - `01` means aligning to the top: the bottom edge will be cut-off if needed
    - `10` means aligning to the bottom: the top edge will be cut-off if needed
    - `11` means aligning to the center: the top and bottom edge will be cut-off equally if needed
  - `EFGH` is reserved for future use and currently **must** be set to `0000`.
