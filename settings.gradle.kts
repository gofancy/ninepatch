rootProject.name = "NinePatch"

pluginManagement {
    repositories {
        gradlePluginPortal()
    }
}

include("awt", "core", "minecraft", "processor")
sequenceOf("android", "stretch-tile").forEach { include("processor:$it")}

sequenceOf<String>().forEach { include("minecraft:v1.$it") }
