# NinePatch
A Modular Java library to allow reading and rendering 9-patch based image files in a way that supports custom use cases.

## Specifications
The concept of nine-patch image is the default one used in other applications. The default format, though, is augmented
in a backwards-compatible manner through the addition of two version tags, that allow further processing.

Given a nine-patch image of width *w* and height *h*, the pixels at coordinates (0, 0) and (*w* - 1, *h* - 1) are
called version tag and sub-version tag, respectively. The version tag is used to identify the kind of nine-patch image
that the library should process, essentially allowing for identification of the target `ImageProcessor`. The sub-version
tag, on the other hand, can be used by the processor to encode additional information for processing, usually additional
specifications that might further identify the type of nine-patch image.

Both version and sub-version tags **must** be dealt with as if they are in RGBA32 format, regardless of the actual pixel
data format of the image. Any necessary conversion from the image pixel format to RGBA32 are thus mandatory.

### Version Tag Specifications
In this piece, an RGBA32 version tag of the form `0xrrggbbaa` is considered.

If the value of `aa` is anything other than `ff`, then the entire version tag shall be treated as if it were
`0xffffff00`, regardless of the actual values present in the version tag. The tag shall then be interpreted according to
the following table, representing all versions mandated by the library:

| Version Tag Form | Type of Image |
|------------------|---------------|
| `0xffffff00`     | Android       |
| `0xffff00ff`     | Stretch/Tile  |

The set of values beginning with `fff` (i.e. `0xfffgbbff`) are reserved for further use. Any other value can be used by
custom processors.

### Sub-version Tag Specifications
In this piece, an RGBA32 version tag of the form `0xrrggbbaa` is considered.

If the value of `aa` is anything other than `ff`, then the entire sub-version tag shall be treated as if it were
`0xffffff00`, regardless of the actual values present in the sub-version tag. Moreover, a sub-version tag of the form
`0x000000ff ` is considered illegal and shall not be used by any processor, unless the processor is the default 9-patch
image processor.

The specific meanings are processor-specific. Refer to the processor-specific set of specifications for more details on
the expected values.
