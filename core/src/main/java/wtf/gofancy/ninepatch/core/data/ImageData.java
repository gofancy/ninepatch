package wtf.gofancy.ninepatch.core.data;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public final class ImageData<I, D> {
    @NotNull private final I image;
    @NotNull private final Size size;
    @NotNull private final ImageDataProvider<D> provider;

    private ImageData(@NotNull final I image, @NotNull final Size size, @NotNull final ImageDataProvider<D> provider) {
        this.image = image;
        this.size = size;
        this.provider = provider;
    }

    @Contract(pure = true, value = "_, _, _ -> new")
    @NotNull
    public static <I, D> ImageData<I, D> of(@NotNull final I image, @NotNull final Size size, @NotNull final ImageDataProvider<D> provider) {
        return new ImageData<>(
                Objects.requireNonNull(image, "image"),
                Objects.requireNonNull(size, "size"),
                Objects.requireNonNull(provider, "provider")
        );
    }

    @Contract(pure = true)
    @NotNull
    public I image() {
        return this.image;
    }

    @Contract(pure = true)
    @NotNull
    public Size size() {
        return this.size;
    }

    @Contract(pure = true)
    @NotNull
    public ImageDataProvider<D> provider() {
        return this.provider;
    }

    @Contract(pure = true)
    @Override
    public String toString() {
        return "Image(%s){%s}".formatted(this.size(), this.image());
    }
}
