package wtf.gofancy.ninepatch.core.data;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.Range;

import java.util.Objects;

public final class Coordinates {
    @Range(from = 0, to = Long.MAX_VALUE) private final long x;
    @Range(from = 0, to = Long.MAX_VALUE) private final long y;

    private Coordinates(@Range(from = 0, to = Long.MAX_VALUE) final long x, @Range(from = 0, to = Long.MAX_VALUE) final long y) {
        this.x = x;
        this.y = y;
    }

    @Contract(pure = true, value = "_, _ -> new")
    public static Coordinates of(@Range(from = 0, to = Long.MAX_VALUE) final long x, @Range(from = 0, to = Long.MAX_VALUE) final long y) {
        return new Coordinates(range(x, "x"), range(y, "y"));
    }

    @Contract(pure = true, value = "_, _ -> param1")
    private static long range(final long l, final String message) {
        if (l < 0) {
            throw new IllegalArgumentException(message);
        }
        return l;
    }

    @Contract(pure = true)
    public long x() {
        return this.x;
    }

    @Contract(pure = true)
    public long y() {
        return this.y;
    }

    @Contract(pure = true, value = "null -> false")
    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        final Coordinates imageSize = (Coordinates) o;
        return this.x() == imageSize.x() && this.y() == imageSize.y();
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.x(), this.y());
    }

    @Contract(pure = true)
    @NotNull
    @Override
    public String toString() {
        return "(%d;%d)".formatted(this.x(), this.y());
    }
}
