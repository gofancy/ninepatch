package wtf.gofancy.ninepatch.core.data;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.Range;

import java.util.Objects;

public final class Size {
    @Range(from = 0, to = Long.MAX_VALUE) private final long width;
    @Range(from = 0, to = Long.MAX_VALUE) private final long height;

    private Size(@Range(from = 0, to = Long.MAX_VALUE) final long width, @Range(from = 0, to = Long.MAX_VALUE) final long height) {
        this.width = width;
        this.height = height;
    }

    @Contract(pure = true, value = "_, _ -> new")
    public static Size of(@Range(from = 0, to = Long.MAX_VALUE) final long width, @Range(from = 0, to = Long.MAX_VALUE) final long height) {
        return new Size(range(width, "width"), range(height, "height"));
    }

    @Contract(pure = true, value = "_, _ -> param1")
    private static long range(final long l, final String message) {
        if (l < 0) {
            throw new IllegalArgumentException(message);
        }
        return l;
    }

    @Contract(pure = true)
    public long width() {
        return this.width;
    }

    @Contract(pure = true)
    public long height() {
        return this.height;
    }

    @Contract(pure = true, value = "null -> false")
    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        final Size size = (Size) o;
        return this.width() == size.width() && this.height() == size.height();
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.width(), this.height());
    }

    @Contract(pure = true)
    @NotNull
    @Override
    public String toString() {
        return "(%d;%d)".formatted(this.width(), this.height());
    }
}
