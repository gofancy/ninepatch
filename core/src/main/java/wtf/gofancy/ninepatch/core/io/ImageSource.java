package wtf.gofancy.ninepatch.core.io;

import org.jetbrains.annotations.NotNull;
import wtf.gofancy.ninepatch.core.data.ImageData;
import wtf.gofancy.ninepatch.core.data.ImagePixelDataFormat;

public interface ImageSource<I, D> {
    @NotNull ImageData<I, D> data();
    @NotNull ImagePixelDataFormat<D> format();
}
