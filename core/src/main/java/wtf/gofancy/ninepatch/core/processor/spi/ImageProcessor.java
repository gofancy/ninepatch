package wtf.gofancy.ninepatch.core.processor.spi;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import wtf.gofancy.ninepatch.core.NinePatchImage;
import wtf.gofancy.ninepatch.core.io.ImageSource;

// スピアヘッドプロセッサ
public interface ImageProcessor {
    interface SubVersionIgnoringImageProcessor extends ImageProcessor {
        @Contract("_ -> new")
        @NotNull
        <I, D> NinePatchImage<I, D> process(@NotNull final ImageSource<I, D> source);

        @Contract("_, _ -> new")
        @NotNull
        default <I, D> NinePatchImage<I, D> process(
                final int rgba32SubVersionTag,
                @NotNull final ImageSource<I, D> source
        ) {
            return this.process(source);
        }
    }

    int rgba32VersionTag();

    @Contract("_, _ -> new")
    @NotNull
    <I, D> NinePatchImage<I, D> process(
            final int rgba32SubVersionTag,
            @NotNull final ImageSource<I, D> source
    );
}
