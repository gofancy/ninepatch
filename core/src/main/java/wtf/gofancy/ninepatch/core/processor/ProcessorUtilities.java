package wtf.gofancy.ninepatch.core.processor;

import org.jetbrains.annotations.Contract;

public final class ProcessorUtilities {
    public static final int RGBA32_BLACK = 0x000000FF;
    public static final int RGBA32_WHITE = 0xFFFFFFFF;

    private ProcessorUtilities() {}

    @Contract(pure = true)
    public static boolean isFullyTransparent(final int rgba32Color) {
        return (rgba32Color & 0x000000FF) == 0x00;
    }

    @Contract(pure = true)
    public static boolean isTransparent(final int rgba32Color) {
        return !isOpaque(rgba32Color);
    }

    @Contract(pure = true)
    public static boolean isOpaque(final int rgba32Color) {
        return (rgba32Color & 0x000000FF) == 0xFF;
    }

    @Contract(pure = true)
    public static boolean matchesColorIgnoringTransparency(final int rgba32Color, final int rgb32Match) {
        return (rgba32Color >>> 8) == (rgb32Match & 0xFFFFFF);
    }

    @Contract(pure = true)
    public static boolean isBlack(final int rgba32Color) {
        return matchesColorIgnoringTransparency(rgba32Color, RGBA32_BLACK >>> 8);
    }

    @Contract(pure = true)
    public static boolean isWhite(final int rgba32Color) {
        return matchesColorIgnoringTransparency(rgba32Color, RGBA32_WHITE >>> 8);
    }

    @Contract(pure = true)
    public static int makeOpaque(final int rgba32Color) {
        return rgba32Color & 0xFFFFFF00;
    }

    @Contract(pure = true)
    public static int makeTransparent(final int rgba32Color) {
        return rgba32Color | 0x000000FF;
    }
}
