package wtf.gofancy.ninepatch.core.data;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.ToIntFunction;

public final class ImagePixelDataFormat<T> {
    @NotNull private final String friendlyName;
    @NotNull private final ToIntFunction<@NotNull T> toRgba32;
    @NotNull private final IntFunction<@NotNull T> fromRgba32;
    @NotNull private final Function<@NotNull T, @NotNull String> toStringFormatter;

    private ImagePixelDataFormat(
            @NotNull final String friendlyName,
            @NotNull final ToIntFunction<@NotNull T> toRgba32,
            @Nullable final IntFunction<@NotNull T> fromRgba32,
            @NotNull final Function<@NotNull T, @NotNull String> toStringFormatter
    ) {
        this.friendlyName = friendlyName;
        this.fromRgba32 = fromRgba32 == null? ImagePixelDataFormat::fail : fromRgba32;
        this.toRgba32 = toRgba32;
        this.toStringFormatter = toStringFormatter;
    }

    @Contract(value = "_, _, _ , _-> new")
    @NotNull
    public static <T extends Number> ImagePixelDataFormat<T> of(
            @NotNull final String friendlyName,
            @NotNull final ToIntFunction<@NotNull T> toRgba32,
            @Nullable final IntFunction<@NotNull T> fromRgba32,
            @NotNull final Function<@NotNull T, @NotNull String> toStringFormatter
    ) {
        return new ImagePixelDataFormat<>(
                Objects.requireNonNull(friendlyName, "friendlyName"),
                Objects.requireNonNull(toRgba32, "toRgba32"),
                fromRgba32,
                Objects.requireNonNull(toStringFormatter, "toStringFormatter")
        );
    }

    @Contract(value = "_ -> fail", pure = true)
    @NotNull
    private static <T> T fail(final int rgba32) {
        throw new UnsupportedOperationException("Cannot obtain a value from RGBA32 as the conversion is lossy");
    }

    @Contract(pure = true)
    public int toRgba32(@NotNull final T data) {
        return this.toRgba32.applyAsInt(Objects.requireNonNull(data, "data"));
    }

    @Contract(pure = true)
    @NotNull
    public T fromRgba32(final int rgba32) {
        return this.fromRgba32.apply(rgba32);
    }

    @Contract(pure = true)
    public String toString(@NotNull final T data) {
        return this.toStringFormatter.apply(Objects.requireNonNull(data, "data"));
    }

    @Contract(pure = true)
    @Override
    public String toString() {
        return this.friendlyName;
    }
}
