package wtf.gofancy.ninepatch.core.processor.internal;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.Unmodifiable;
import wtf.gofancy.ninepatch.core.NinePatchImage;
import wtf.gofancy.ninepatch.core.data.CommonPixelDataFormats;
import wtf.gofancy.ninepatch.core.data.Coordinates;
import wtf.gofancy.ninepatch.core.data.ImageData;
import wtf.gofancy.ninepatch.core.data.ImageDataProvider;
import wtf.gofancy.ninepatch.core.data.Size;
import wtf.gofancy.ninepatch.core.io.ImageSource;
import wtf.gofancy.ninepatch.core.processor.UnknownNinepatchVersionTagException;
import wtf.gofancy.ninepatch.core.processor.spi.ImageProcessor;

import java.util.Collections;
import java.util.Map;

public final class NinePatchReader {
    private static final class LazyToken {
        static final NinePatchReader INSTANCE = new NinePatchReader(ProcessorDiscovery.discoverProcessors());
    }

    @NotNull @Unmodifiable private final Map<Integer, ImageProcessor> processors;

    private NinePatchReader(@NotNull final Map<Integer, ImageProcessor> processors) {
        this.processors = Collections.unmodifiableMap(processors);
    }

    @NotNull
    public static NinePatchReader get() {
        return LazyToken.INSTANCE;
    }

    @Contract("_ -> new")
    @NotNull
    public <I, D> NinePatchImage<I, D> read(@NotNull final ImageSource<I, D> source) {
        final long tags = this.extractVersionSubVersionTags(source);

        final int versionTag = this.validateSpec(SpecBasedTags.treatVersionTag((int) (tags >>> 32)), "");
        final int subVersionTag = this.validateSpec(SpecBasedTags.treatSubVersionTag(versionTag, (int) tags), "sub-");

        final ImageProcessor processor = this.processors.get(versionTag);
        return this.tryDispatchToProcessor(processor, versionTag, subVersionTag, source);
    }

    private <I, D> long extractVersionSubVersionTags(@NotNull final ImageSource<I, D> source) {
        final ImageData<I, D> data = source.data();
        final Size size = data.size();
        final ImageDataProvider<D> provider = data.provider();

        final long rgba32VersionTag = provider.rgba32PixelAt(Coordinates.of(0, 0));
        final long rgba32SubVersionTag = provider.rgba32PixelAt(Coordinates.of(size.width() - 1, size.height() - 1));

        return (rgba32VersionTag << 32) | rgba32SubVersionTag;
    }

    private int validateSpec(final long validation, @NotNull final String m) {
        final int data = (int) validation;
        final boolean valid = ((validation >>> 32) & 1) != 0;
        if (!valid) {
            throw new IllegalArgumentException("Invalid " + m + "version tag " + CommonPixelDataFormats.RGBA32.toString(data));
        }
        return data;
    }

    @Contract("null, _, _, _ -> fail; !null, _, _, _ -> new")
    @NotNull
    private <I, D> NinePatchImage<I, D> tryDispatchToProcessor(
            @Nullable final ImageProcessor processor,
            final int versionTag,
            final int subVersionTag,
            @NotNull final ImageSource<I, D> source
    ) {
        if (processor == null) {
            throw new UnknownNinepatchVersionTagException(versionTag);
        }
        return this.dispatchToProcessor(processor, subVersionTag, source);
    }

    @Contract("_, _, _ -> new")
    @NotNull
    private <I, D> NinePatchImage<I, D> dispatchToProcessor(
            @NotNull final ImageProcessor processor,
            final int subVersionTag,
            @NotNull final ImageSource<I, D> source
    ) {
        return processor.process(subVersionTag, source);
    }
}
