package wtf.gofancy.ninepatch.core;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import wtf.gofancy.ninepatch.core.data.ImageData;
import wtf.gofancy.ninepatch.core.data.Size;
import wtf.gofancy.ninepatch.core.io.ImageSource;
import wtf.gofancy.ninepatch.core.io.Surface;
import wtf.gofancy.ninepatch.core.processor.internal.NinePatchReader;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public interface NinePatchImage<I, D> {
    @Contract("_ -> new")
    @NotNull
    static <I, D> NinePatchImage<I, D> read(@NotNull final ImageSource<I, D> source) {
        return NinePatchReader.get().read(Objects.requireNonNull(source, "source"));
    }

    @NotNull ImageData<I, D> data();

    default void draw(@NotNull final Surface<I, D> surface) {
        this.drawFittingSize(surface, surface.fullSize());
    }

    void drawFittingSize(@NotNull final Surface<I, D> surface, @NotNull final Size desiredSize);

    default void drawFittingContent(@NotNull final Surface<I, D> surface, @NotNull final Size desiredContentSize) {
        this.drawFittingContents(surface, Collections.singletonList(desiredContentSize));
    }

    void drawFittingContents(@NotNull final Surface<I, D> surface, @NotNull final List<Size> desiredContentSizes);
}
