package wtf.gofancy.ninepatch.core.processor.internal;

import org.jetbrains.annotations.Contract;
import wtf.gofancy.ninepatch.core.processor.ProcessorUtilities;

final class SpecBasedTags {
    @Contract(pure = true)
    static long treatVersionTag(final int versionTag) {
        if (ProcessorUtilities.isTransparent(versionTag)) {
            return 0x01FFFFFF00L;
        }
        return (0x01L << 32) | ProcessorUtilities.makeOpaque(versionTag);
    }

    @Contract(pure = true)
    static long treatSubVersionTag(final int versionTag, final int subVersionTag) {
        if (ProcessorUtilities.isTransparent(versionTag)) {
            return (0x01L << 32) | subVersionTag;
        }
        if (ProcessorUtilities.isTransparent(subVersionTag)) {
            return 0x01FFFFFF00L;
        }
        final int opaqueVersionTag = ProcessorUtilities.makeOpaque(subVersionTag);
        final boolean isValid = !ProcessorUtilities.isBlack(opaqueVersionTag);
        return ((isValid? 0x01L : 0x00L) << 32) | opaqueVersionTag;
    }
}
