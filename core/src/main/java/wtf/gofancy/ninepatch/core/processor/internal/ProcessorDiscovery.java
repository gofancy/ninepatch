package wtf.gofancy.ninepatch.core.processor.internal;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Unmodifiable;
import wtf.gofancy.ninepatch.core.NinePatchImage;
import wtf.gofancy.ninepatch.core.data.CommonPixelDataFormats;
import wtf.gofancy.ninepatch.core.io.ImageSource;
import wtf.gofancy.ninepatch.core.processor.spi.ImageProcessor;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.ServiceLoader;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

// パラレイド
final class ProcessorDiscovery {
    @SuppressWarnings("ClassCanBeRecord") // Java 8 support
    private static final class FilteredProcessor implements ImageProcessor {
        @NotNull private final ImageProcessor delegate;

        private FilteredProcessor(@NotNull final ImageProcessor delegate) {
            this.delegate = delegate;
        }

        @NotNull
        static ImageProcessor filter(@NotNull final ImageProcessor processor) {
            return new FilteredProcessor(Objects.requireNonNull(processor, "processor"));
        }

        @Override
        public int rgba32VersionTag() {
            return this.filterVersionTag(this.delegate.rgba32VersionTag());
        }

        @Contract(value = "_, _ -> new")
        @NotNull
        @Override
        public <I, D> NinePatchImage<I, D> process(final int rgba32SubVersionTag, @NotNull final ImageSource<I, D> source) {
            return this.delegate.process(this.filterSubVersionTag(rgba32SubVersionTag), source);
        }

        @Contract(pure = true)
        private int filterVersionTag(final int versionTag) {
            return (int) SpecBasedTags.treatVersionTag(versionTag);
        }

        private int filterSubVersionTag(final int subVersionTag) {
            return (int) SpecBasedTags.treatSubVersionTag(this.rgba32VersionTag(), subVersionTag);
        }
    }

    @Unmodifiable
    @NotNull
    static Map<Integer, ImageProcessor> discoverProcessors() {
        return Collections.unmodifiableMap(discoverAndFilterProcessors());
    }

    @NotNull
    private static Map<Integer, ImageProcessor> discoverAndFilterProcessors() {
        return StreamSupport.stream(ServiceLoader.load(ImageProcessor.class).spliterator(), false)
                .unordered()
                .map(FilteredProcessor::filter)
                .collect(Collectors.toMap(
                        ImageProcessor::rgba32VersionTag,
                        Function.identity(),
                        ProcessorDiscovery::fail,
                        HashMap::new
                ));
    }

    @Contract("_, _ -> fail")
    private static ImageProcessor fail(final ImageProcessor a, final ImageProcessor b) {
        final String version = CommonPixelDataFormats.RGBA32.toString(a.rgba32VersionTag());
        throw new UnsupportedOperationException("Conflict between providers " + a + " and " + b + ": both accept version " + version);
    }
}
