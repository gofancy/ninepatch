package wtf.gofancy.ninepatch.core.io;

import org.jetbrains.annotations.NotNull;
import wtf.gofancy.ninepatch.core.data.Coordinates;
import wtf.gofancy.ninepatch.core.data.ImageData;
import wtf.gofancy.ninepatch.core.data.ImagePixelDataFormat;
import wtf.gofancy.ninepatch.core.data.Size;

public interface Surface<I, D> {
    @NotNull ImagePixelDataFormat<D> format();
    @NotNull Size fullSize();

    void drawSection(
            @NotNull final ImageData<I, D> imageData,
            @NotNull final Coordinates sectionLocation,
            @NotNull final Size sectionSize,
            @NotNull final Coordinates surfaceLocation,
            @NotNull final Size surfaceSize
    );
}
