package wtf.gofancy.ninepatch.core.processor;

import wtf.gofancy.ninepatch.core.data.CommonPixelDataFormats;

public final class UnknownNinepatchSubVersionTagException extends IllegalArgumentException {
    public UnknownNinepatchSubVersionTagException(final int rgba32SubVersionTag) {
        super("Unknown sub-version tag for image: " + CommonPixelDataFormats.RGBA32.toString(rgba32SubVersionTag));
    }
}
