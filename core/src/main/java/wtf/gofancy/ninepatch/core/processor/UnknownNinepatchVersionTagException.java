package wtf.gofancy.ninepatch.core.processor;

import wtf.gofancy.ninepatch.core.data.CommonPixelDataFormats;

public final class UnknownNinepatchVersionTagException extends IllegalArgumentException {
    public UnknownNinepatchVersionTagException(final int rgba32VersionTag) {
        super("Unknown version tag for image: " + CommonPixelDataFormats.RGBA32.toString(rgba32VersionTag));
    }
}
