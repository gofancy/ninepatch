package wtf.gofancy.ninepatch.core.data;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public interface ImageDataProvider<D> {
    @FunctionalInterface
    interface FormattedImageDataProvider<D> {
        @NotNull D pixelAt(@NotNull final Coordinates coordinates);
    }

    @Contract(pure = true, value = "_, _ -> new")
    @NotNull
    static <D> ImageDataProvider<D> of(@NotNull final ImagePixelDataFormat<D> format, @NotNull final FormattedImageDataProvider<D> provider) {
        Objects.requireNonNull(format, "format");
        Objects.requireNonNull(provider, "provider");

        return new ImageDataProvider<D>() {
            @Override
            public @NotNull D formattedPixelAt(@NotNull final Coordinates coordinates) {
                return provider.pixelAt(coordinates);
            }

            @Override
            public int rgba32PixelAt(@NotNull final Coordinates coordinates) {
                return format.toRgba32(this.formattedPixelAt(coordinates));
            }
        };
    }

    @NotNull D formattedPixelAt(@NotNull final Coordinates coordinates);
    int rgba32PixelAt(@NotNull final Coordinates coordinates);
}
