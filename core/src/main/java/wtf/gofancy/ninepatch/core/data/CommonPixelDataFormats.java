package wtf.gofancy.ninepatch.core.data;

import org.jetbrains.annotations.NotNull;

public final class CommonPixelDataFormats {
    public static final ImagePixelDataFormat<Integer> RGB32 = ImagePixelDataFormat.of(
            "rgb32",
            it -> it << 8,
            it -> it >>> 8,
            it -> formatRgba32(it << 8)
    );
    public static final ImagePixelDataFormat<Integer> RGBA32 = ImagePixelDataFormat.of(
            "rgba32",
            it -> it,
            it -> it,
            CommonPixelDataFormats::formatRgba32
    );

    private CommonPixelDataFormats() {}

    @NotNull
    private static String formatRgba32(final int rgba32) {
        return String.format("0x%8x", rgba32);
    }
}
