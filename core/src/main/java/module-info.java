module wtf.gofancy.ninepatch.core {
    requires static org.jetbrains.annotations;

    exports wtf.gofancy.ninepatch.core;
    exports wtf.gofancy.ninepatch.core.data;
    exports wtf.gofancy.ninepatch.core.io;
    exports wtf.gofancy.ninepatch.core.processor;
    exports wtf.gofancy.ninepatch.core.processor.spi;

    uses wtf.gofancy.ninepatch.core.processor.spi.ImageProcessor;
}
