plugins {
    id("wtf.gofancy.ninepatch.gradle.java-conventions")
}

ninepatch {
    projectId.set("NinePatch")
    mainPackage.set("ninepatch")
}
