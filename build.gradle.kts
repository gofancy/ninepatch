plugins {
    id("wtf.gofancy.ninepatch.gradle.core")
}

repositories {}

dependencies {}

tasks.withType<Wrapper> {
    gradleVersion = "7.5"
    distributionType = Wrapper.DistributionType.ALL
}
